var museum_map = null;
var museum_marker = null;
var museum_geocoder = new google.maps.Geocoder();

jQuery(document).ready(function(){
  // draw a map
  if( jQuery('#museums_map_canvas').length > 0 ) draw_map( 'museums_map_canvas' ); 

	if ( ! window.navigator ) {
		jQuery('#current-location').addClass('hide');
	}

	jQuery('#cps-get-current-location').click(function(){
		geolocationGathering();
	});

	jQuery('#cps-update-ds-account').click(function(e){
		e.preventDefault();
		jQuery.post(
			ajaxurl
			,{
				action: 'ds_update_account'
				,data: {
					username: jQuery('#cps_ds_username').val()
					, password: jQuery('#cps_ds_password').val()
				}
			}
			, function( resp ) {
				if ( ( 'success' == resp.status ) && ( jQuery('#cps_ds_username').val() == resp.message.username ) ) {
					jQuery('#cps-test-connection-status')
						.removeClass('cps-loading')
						.removeClass('hide')
						.text('Saved');
				}
			}
		);

	});

	jQuery('#cps-test-connection').click(function(e){
		e.preventDefault();

		jQuery('#cps-test-connection-status').removeClass('hide');

		jQuery.post(
			ajaxurl
			,{
				action: 'ds_authentication'
				,data: { 
					username: jQuery('#cps_ds_username').val()
					, password: jQuery('#cps_ds_password').val() 
				}
			}
			, function( resp ) {

				console.log( resp );

				jQuery('#cps-test-connection-status').removeClass('cps-loading');

				if ( ( 'success' == resp.status ) && ( jQuery('#cps_ds_username').val() == resp.message.username ) ) {
					jQuery('#cps-test-connection-status').text('Connected');
				} else {
					jQuery('#cps-test-connection-status').text('Failed');
				}

			}
		);
	});

	jQuery('#remove-featured-image').click(function(e){
		e.preventDefault();

		if ( confirm( jQuery(this).data('confirm') ) ) {
			window.location = jQuery(this).attr('href');
		}
	});

});

function geolocationGathering() {
	if ( ! window.navigator ) {
		alert('Your browser does not support navigator API');
		return;
	}

	jQuery('#get-current-location').removeClass('hide');

	navigator.geolocation.getCurrentPosition(
		function( position ){
			// location return	
			lat = position.coords.latitude;
			lng = position.coords.longitude;

			jQuery('#museum_location_lat').val( lat );
			jQuery('#museum_location_lng').val( lng );

			var newCentre = new google.maps.LatLng( lat, lng, true );

			museum_map.setZoom( 14 );
			museum_map.panTo( newCentre );
			museum_marker.setPosition( newCentre );

			museum_geocoder = museum_geocoder ? museum_geocoder : new google.maps.Geocoder() ;

			museum_geocoder.geocode({
				location: museum_marker.getPosition()	
			}
			, function( codeResult, codeStatus ){
				if ( google.maps.GeocoderStatus.OK == codeStatus ) {
					jQuery('#museum_location_desc').val( codeResult[0].formatted_address );
				}
			});

		}
		, function(){
			// No location	
		}
	);

	jQuery('#get-current-location').addClass('hide');
}

function draw_map( map_id ) {
	var latlng = null;

	if ( ( lat = jQuery( '#' + map_id ).data( 'latitude' ) ) && ( lng = jQuery( '#' + map_id ).data( 'longitude' ) ) ) {
		latlng = new google.maps.LatLng( jQuery( '#' + map_id ).data( 'latitude' ), jQuery( '#' + map_id ).data( 'longitude' ) );

		jQuery('#museum_location_lat').val( jQuery( '#' + map_id ).data( 'latitude' ) );
		jQuery('#museum_location_lng').val( jQuery( '#' + map_id ).data( 'longitude' ) );

	} else {
		latlng = new google.maps.LatLng( 13.75, 100.517 );
	}

	var map_options = {
		center: latlng
		, zoom: 8
		, mapTypeId: google.maps.MapTypeId.ROADMAP
	}

	museum_map = new google.maps.Map( document.getElementById( map_id ), map_options );

	museum_marker = new google.maps.Marker({
		position: map_options.center	
		, map: museum_map
		, draggable: true
		, animation: google.maps.Animation.DROP
	});

	google.maps.event.addListener(museum_marker, 'dragend', function(){
		jQuery('#museum_location_lat').val( museum_marker.getPosition().lat() );
		jQuery('#museum_location_lng').val( museum_marker.getPosition().lng() );

		museum_geocoder.geocode({
			location: museum_marker.getPosition()	
		}
		, function( codeResult, codeStatus ){
			if ( google.maps.GeocoderStatus.OK == codeStatus ) {
				jQuery('#museum_location_desc').val( codeResult[0].formatted_address );
			}
		});
	});
	
}
