<?php

class Admin_Statistic_Counting {

    const STATISTICS_COUNTING_UNKNOWN_TYPE                  = 0;
    const STATISTICS_COUNTING_CHECKIN_TYPE                  = 1;
    const STATISTICS_COUNTING_FETCHED_ITEM_INFORMATION_TYPE = 2;

    public static function increaseViewNumberForItem( $itemId = 0, $userId = 0 ) {
        global $wpdb;

        if ( $itemId && $userId ) {
            $museumId = get_option( 'ds_museum_id' );
            $itemTitle = get_the_title( $itemId );

            $number = $wpdb->insert(
                $wpdb->statistics,
                array(
                    'user_id'   => (int) $userId,
                    'place_id'  => (int) $museumId,
                    'item_id'   => (int) $itemId,
                    'item_name' => $itemTitle,
                    'statistics_type'   => static::STATISTICS_COUNTING_FETCHED_ITEM_INFORMATION_TYPE
                )
            );

            if ( $number ) {
                $item_number = get_post_meta( $itemId, 'item_countable_view', true );
                if ( 0 < intval( $item_number ) ) {
                    $item_number++;
                    update_post_meta( $itemId, 'item_countable_view', $item_number );
                } else {
                    add_post_meta( $itemId, 'item_countable_view', 1 );
                }
            }
        }
    }

    public static function increaseCheckInNumberForMuseum( $userId = 0 ) {
        global $wpdb;

        if ( $userId ) {
            $museumId = get_option( 'ds_museum_id' );

            $wpdb->insert(
                $wpdb->statistics,
                array(
                    'user_id'   => (int) $userId,
                    'place_id'  => (int) $museumId,
                    'statistics_type'   => static::STATISTICS_COUNTING_CHECKIN_TYPE
                )
            );
        }
    }
}
