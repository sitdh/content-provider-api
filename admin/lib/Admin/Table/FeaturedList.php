<?php

if ( ! class_exists( 'WP_List_Table' ) ) 
    require_once ABSPATH . '/wp-admin/includes/class-wp-list-table.php';

class Admin_Table_FeaturedList extends WP_List_Table {

    public function __construct() {
        parent::__construct( array(
            'plural'    => 'items',
            'singular'  => 'item'
        ) );
    }

    public function get_columns() {
        $columns = array(
                'cb'        => '<input type="checkbox" />',
                'itemtitle' => __( 'Title', 'museums_network' ),
                'frequency' => __( 'Frequency', 'museums_network' )
            );

        return $columns;
    }

    public function query_stat( $for_period = 0 ) {
        global $post, $wpdb;
        
        $items = $wpdb->get_results(
            "
                SELECT item_id, item_name, COUNT(item_id) as item_mean
                FROM {$wpdb->statistics}
                GROUP BY item_id
                ORDER BY item_mean DESC
            "
        );

        $itemsResponse = array();

        foreach( $items as $item ) {
            $post = get_post( $item->item_id );

            if ( $post ) {
                setup_postdata( $post );
                
                $itemTemp = array();

                $itemTemp['itemtitle'] = get_the_title();
                $itemTemp['itemId'] = get_the_ID();
                $itemTemp['frequency'] = $item->item_mean;

                $itemsResponse[] = $itemTemp;
            }
        }

        wp_reset_postdata();

        return $itemsResponse;
    }

    public function prepare_items() {
        $columns = $this->get_columns();
        $hidden = array( 'itemId' );
        $sortable = array();
        $this->_column_headers = array( $columns, $hidden, $sortable );

        $this->items = $this->query_stat();

        $per_page = 10;
        $current_page = $this->get_pagenum();
        $total_items = count( $this->items );

        $this->set_pagination_args( array(
            'total_items'   => $total_items,
            'per_page'      => $per_page
        ) );
    }

    public function column_default( $item, $column_name ) {
        $value = null;

        switch( $column_name ) {
            case 'frequency':
                $value = $item[ $column_name ];
                break;

            default:
                $value = '-';
        }

        return $value;
    }

    public function column_cb( $item ) {
        return sprintf( 
            '<input type="checkbox" name="%s[]" value="%s" />',
            $this->_args['singular'],
            $item['itemId']
        );
    }

    public function column_itemtitle( $item ) {
        $actions = array( 
            'edit'  => sprintf( 
                        '<a href="?page=%s&action=edit&item=%s',
                        $item['itemTitle'],
                        $item['itemId']
                    ),
            'delete'=> sprintf( 
                        '<a href="?page=%s&action=delete&item=%s',
                        $item['itemTitle'],
                        $item['itemId']
                    ),
        );

        return sprintf( 
            '<a href="#">%s</a> %s', 
            $item['itemtitle'],
            $this->row_actions( $actions, true ) 
        );

    }

    public function no_items() {
        _e( 'There are no statistic for this museum yet', 'museums_network' );
    }

    public function get_bulk_actions() {
        $actions = array(
            'a' => __( 'A', 'museums_network' )
        );

        return $actions;
    }
}
