<?php

require_once 'Admin/Interfaces.php';

require_once 'Admin/Table/FeaturedList.php';

class Admin_Interfaces_Statistics extends Admin_Interfaces {

	public function __construct() {
		$this->interface_name = 'Statistics';
	}

    public function query_datasource() {
        global $wpdb;

        $filter_type = in_array( @$_REQUEST['filter'], array( 'weekly', 'monthly', 'all' ) ) ? $_GET['filter'] : '' ;

        $sql = '';
        if ( 'weekly' == $filter_type ) {
            $sql = "SELECT DATE_FORMAT( checked_in_timestamp_gmt, '%W' ) as item_title, COUNT( item_id ) AS number_of_item
                FROM {$wpdb->statistics}
                WHERE statistics_type = 2 
                    AND TIMESTAMPDIFF(YEAR, checked_in_timestamp_gmt, CURRENT_DATE) < 2
                GROUP BY item_title
                ORDER BY item_title ASC";

        } elseif ( 'monthly' == $filter_type ) {
            $sql = $wpdb->prepare(
                "SELECT MONTHNAME( checked_in_timestamp_gmt ) as item_title, COUNT( item_id ) AS number_of_item
                FROM {$wpdb->statistics}
                WHERE statistics_type = %d
                    AND TIMESTAMPDIFF(YEAR, checked_in_timestamp_gmt, CURRENT_DATE) < 2
                GROUP BY item_title
                ORDER BY item_title ASC",
                2
            );

        } else {
            $sql = $wpdb->prepare(
                "SELECT DATE( checked_in_timestamp_gmt ) as item_title, COUNT( item_id ) AS number_of_item
                FROM {$wpdb->statistics}
                WHERE statistics_type = %d
                    AND TIMESTAMPDIFF(DAY, checked_in_timestamp_gmt, CURRENT_DATE) < 120
                GROUP BY item_title
                ORDER BY item_title ASC",
                2
            );

        }

        $dataSource = $wpdb->query( $sql );
        
        $itemPacked = array();
        if ( $dataSource ) {
            $dataSource = $wpdb->get_results( $sql );
            foreach( $dataSource as $item ) {
                $itemPacked[] = array( $item->item_title, intval( $item->number_of_item ) );
            }
        }
        
        return $itemPacked;
    }

	public function render() {
		$current = array();
		if ( isset( $_GET['filter'] ) ) $current[ $_GET['filter'] ] = 'current' ;

        $itemsTable = new Admin_Table_FeaturedList();
        $itemsTable->prepare_items();

        $this->query_datasource();

	?>
		<h2><?php _e( 'Statistics', 'museums_network' ); ?></h2>

		<!-- #cps_grahp_canvas.graph-canvas -->
		<div id="cps_grahp_canvas" class="graph-canvas wrap">
			<div class="wrap">
				<!-- #cps_statistic_filter .subsubsub -->
				<ul id="cps_statistic_filter" class="subsubsub">
					<li>
						<!-- .cps-statistic-filter-lable -->
						<a alt="<?php _e('Daily', 'museums_network'); ?>" href="?page=content-provider-menu&tab=cps-statistics" class="cps-statistic-filter-lable<?php echo empty( $current ) ? ' current' : '' ; ?>">
							<?php _e('All', 'museums_network'); ?>	
						</a>
						<!-- /.cps-statistic-filter-lable -->	
						|
					</li>
					<li>
						<!-- .cps-statistic-filter-lable -->
					<a alt="<?php _e('Weekly', 'museums_network'); ?>" href="?page=content-provider-menu&tab=cps-statistics&filter=weekly" class="cps-statistic-filter-lable<?php echo " {$current['weekly']}"; ?>">
							<?php _e('Weekly', 'museums_network'); ?>	
						</a>
						<!-- /.cps-statistic-filter-lable -->	
						|
					</li>
					<li>
						<!-- .cps-statistic-filter-lable -->
						<a alt="<?php _e('Monthly', 'museums_network'); ?>" href="?page=content-provider-menu&tab=cps-statistics&filter=monthly" class="cps-statistic-filter-lable<?php echo " {$current['monthly']}"; ?>">
							<?php _e('Monthly', 'museums_network'); ?>	
						</a>
						<!-- /.cps-statistic-filter-lable -->	
					</li>
				</ul>
				<!-- /#cps_statistic_filter .subsubsub -->
			</div>

			<!-- .clearfix -->
			<div class="clearfix"></div>
			<!-- /.clearfix -->

            <?php 
                $dataSource =  $this->query_datasource();
                if ( $dataSource ) : ?>
                <script type="text/javascript">
                    jQuery(document).ready(function(){
                        jQuery.jqplot.config.enablePlugins = true;
                        var s1 = <?php echo json_encode( $dataSource ) ; ?>;
                        jQuery('#graph_wrapper').jqplot([s1], {
                            title: '<?php _e( 'Statistics', 'museums_network' ); ?>',
                            animate: !jQuery.jqplot.use_excanvas,
                            highlighter: {show: false},
                            seriesDefaults: {
                               renderer: jQuery.jqplot.BarRenderer,
                               pointLabels: {show: true}
                            },
                            axes: {
                                xaxis: {
                                    renderer: jQuery.jqplot.CategoryAxisRenderer
                                }
                            }
                        });
                    }); 
                
                </script>

                <!-- #graph_wrapper.wrap -->
                <div id="graph_wrapper" class="wrap">
                </div>
                <!-- /#graph_wrapper.wrap -->
            <?php else : ?>
                <!-- #graph_wrapper.wrap -->
                <div id="graph_wrapper" class="wrap" style="background-color: #80868a; height: 250px; line-height: 250px; text-align: center;">
                    <h1><?php _e( 'Data not available', 'museums_network' ); ?></h1>	
                </div>
                <!-- /#graph_wrapper.wrap -->
            <?php endif; ?>
		</div>
		<!-- /#cps_grahp_canvas.graph-canvas -->

        <!-- .clearfix -->
        <div class="clearfix"></div>
        <!-- /.clearfix -->

        <!-- #cps_table_item_order_list.wrap -->
        <div id="cps_table_item_order_list" class="wrap">
            <style type="text/css">
                .column-frequency { width: 15%; text-align: right; }
            </style>
            <h2><?php _e( 'Items', 'museums_network' ); ?></h2> 
            <?php $itemsTable->display(); ?>
        </div>
        <!-- /#cps_table_item_order_list.wrap -->
	<?php

	}
}
