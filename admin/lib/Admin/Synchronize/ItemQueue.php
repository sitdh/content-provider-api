<?php

require_once 'museums-network/Admin/Informations/Synchronize/Featured.php';

class Admin_Synchronize_ItemQueue {

    protected $featuredImage = null;

    public function __construct() {
        $this->featuredImage = Admin_Informations_Synchronize_Featured::getInstance();
    }
    
    public static function cleanUp() {
        global $wpdb, $post;

        $time = time();
        update_option( 'aqx', $time );

        // Update 
        $queued_items = query_posts(array(
                'meta_query'    => array(
                        'relation'  => 'AND',
                        array(
                            'key'       => '_cps_item_update_status',
                            'value'     => CPS_Sync_Status::CPS_ITEM_STATUS_QUEUE_UP,
                            'compare'   => '='
                        ),
                        array(
                            'key'       => '_cps_item_action',
                            'value'     => 'update',
                            'compare'   => '='
                        )
                    )
            ));
        
        if ( ! empty( $queued_items ) ) {
            $featuredSync = new Admin_Synchronize_ItemQueue();
            foreach( $queued_items as $item ) {
                $featuredSync->synchronized_information( $item );
            }
        }
        
    }

	public function synchronized_information( $featured_item_id ) {
		global $post;

        $post = is_numeric( $featured_item_id ) ? get_post( $featured_item_id ) : $featured_item_id ;

		if ( $post ) {

			update_post_meta( 
				$featured_item_id, 
				'_cps_item_update_status', 
				CPS_Sync_Status::CPS_ITEM_STATUS_SYNC_IN_PROGRESS 
			);

			$client = new IXR_Client( DISTRIBUTOR_SERVER );
			$client->debug = false;

			setup_postdata( $post );

			if ( has_post_thumbnail() ) {
                $image_id = Admin_Informations_Synchronize_Featured::getInstance()
                    ->update_featured_image( get_post_thumbnail_id( get_the_ID() ), $client );
				add_post_meta( get_the_ID(), 'ds_server_image_id', $image_id );
			}

			$credentials = get_option( 'ds_account' );

			$featuredOptions = array();

			// 0: Username
			$featuredOptions[] = $credentials['username'];

			// 1: Password
			$featuredOptions[] = $credentials['password'];

			// 2: Title
			$featuredOptions[] = get_the_title();
			
			// 3: Content
			$content = get_the_content();
			$content = strip_tags( $content );
			$featuredOptions[] = mb_substr( $content, 0, 500 );

			// 4: Belong to museum id
			$featuredOptions[] = get_option( 'ds_museum_id' );

			// 5: Thumbnail Id
			$featuredOptions[] = $image_id;

			// 6: Exists id
			$featuredOptions[] = get_post_meta( get_the_ID(), 'ds_server_item_id', true );

			$client->query(
				'mp.updateFeaturedItem',
				$featuredOptions
			);

			$updateResult = $client->getResponse();

			$updateStatus = CPS_Sync_Status::CPS_ITEM_STATUS_SYNC_IN_PROGRESS;
			if ( 'success' == $updateResult['status'] ) {

				$serverItemId = $updateResult['message']['item_id'];
				update_post_meta( get_the_ID(), 'ds_server_item_id', $serverItemId );

				$updateStatus = CPS_Sync_Status::CPS_ITEM_STATUS_ALREADY_SYNC; // Success

				$sync_date = date( sprintf( 
					'%s %s',
					get_option( 'date_format' ),
					get_option( 'time_format' )
				) );
				update_post_meta( get_the_ID(), '_cps_sync_timestamp', $sync_date );

			} else {
				$updateStatus = CPS_Sync_Status::CPS_ITEM_STATUS_SYNC_ERROR; // Postpone
			}

			update_post_meta( get_the_ID(), '_cps_item_update_status', $updateStatus );

		} else {
			update_post_meta( 
				$featured_item_id, 
				'_cps_item_update_status', 
				CPS_Sync_Status::CPS_ITEM_STATUS_SYNC_ERROR 
			);
		}

		wp_reset_query();
		wp_reset_postdata();
	}
}
