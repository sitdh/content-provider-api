<?php

abstract class Admin_Interfaces {
	private static $instances = array();

	private $interface_name;

	private $interface_slug;

	/**
	 * Use for render page 
	 *
	 * @return page UI
	 **/
	abstract public function render();

	/**
	 * Create class instance
	 *
	 * @return class instance
	 **/
	public static function getInstance() {
		$class = get_called_class();
		if ( ! isset( self::$instances[$class] ) ) 
			self::$instances[$class] = new $class();

		return self::$instances[$class];
	}

	public function interfaceName() {
		return $this->interfaceName;
	}

	public function interfaceSlug() {
		return $this->interfaceSlug;
	}


}
