<?php

set_include_path( get_include_path() . PATH_SEPARATOR . dirname( __FILE__ ) . '/lib/' );

require_once 'museums-network/Admin/Informations/ClientMuseumDataManager.php';
require_once 'museums-network/Admin/Informations/FeaturedItemList.php';
require_once 'Admin/Interfaces/Statistics.php';

function load_custom_scripts() {
    if ( 'cps-statistics' == $_GET['tab'] ) {
        wp_enqueue_style(
            'jqplot_style',
            plugins_url( 'admin/assets/venders/jqplot/jquery.jqplot.min.css', dirname( __FILE__ ) ),
            array(),
            '1.0.8'
        );

        wp_enqueue_script(
            'jqplot_script',
            plugins_url( 'admin/assets/venders/jqplot/jquery.jqplot.min.js', dirname( __FILE__ ) ),
            array( 'jquery' ),
            '1.0.8',
            true
        );

        wp_enqueue_script(
            'jqplot_renderer',
            plugins_url( 'admin/assets/venders/jqplot/plugins/jqplot.barRenderer.min.js', dirname( __FILE__ ) ),
            array( 'jqplot_script' ),
            '1.0.8',
            true
        );

        wp_enqueue_script(
            'jqplot_category_axis',
            plugins_url( 'admin/assets/venders/jqplot/plugins/jqplot.categoryAxisRenderer.min.js', dirname( __FILE__ ) ),
            array( 'jqplot_script' ),
            '1.0.8',
            true
        );

        wp_enqueue_script(
            'jqplot_point_labels',
            plugins_url( 'admin/assets/venders/jqplot/plugins/jqplot.pointLabels.min.js', dirname( __FILE__ ) ),
            array( 'jqplot_script' ),
            '1.0.8',
            true
        );

        wp_enqueue_script(
            'jqplot_cursor',
            plugins_url( 'admin/assets/venders/jqplot/plugins/jqplot.cursor.min.js', dirname( __FILE__ ) ),
            array( 'jqplot_script' ),
            '1.0.8',
            true
        );
        wp_enqueue_script(
            'jqplot_date_axis',
            plugins_url( 'admin/assets/venders/jqplot/plugins/jqplot.dateAxisRenderer.min.js', dirname( __FILE__ ) ),
            array( 'jqplot_script' ),
            '1.0.8',
            true
        );
    }

}
add_action( 'admin_enqueue_scripts', 'load_custom_scripts' );

function cps_plugin_menus() {
	add_options_page(
		__('Content Provider Configurations', 'museums_network'),
		__('Content Provider', 'museums_network'),
		'manage_options',
		'content-provider-menu',
		'content_provider_server_menu'
	);

}
add_action( 'admin_menu', 'cps_plugin_menus' );

function content_provider_server_menu() {

	do_action('cps_admin_scripts');

	do_action('cps_admin_styles');

	$current_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : "cps-general" ; ?>

	<!-- .wrap -->
	<div class="wrap">
		<!-- #icon-options-general.icon32 -->
		<div id="icon-options-general" class="icon32"><br /></div>
		<!-- /#icon-options-general.icon32 -->
		
		<!-- .nav-tab-wrapper -->
		<h2 class="nav-tab-wrapper">
			<!-- .nav-tab -->
			<a href="?page=content-provider-menu&tab=cps-general" class="nav-tab<?php echo ( 'cps-general' == $current_tab ) ? ' nav-tab-active' : '' ; ?>">
				<?php _e( 'Settings', 'museums_network' ); ?>
			</a>
			<!-- /.nav-tab -->

			<!-- .nav-tab -->
			<a href="?page=content-provider-menu&tab=cps-featured" class="nav-tab<?php echo ( 'cps-featured' == $current_tab ) ? ' nav-tab-active' : '' ; ?>">
				<?php _e( 'Featured Items', 'museums_network' ); ?>
			</a>
			<!-- /.nav-tab -->

			<!-- .nav-tab -->
			<a href="?page=content-provider-menu&tab=cps-statistics" class="nav-tab<?php echo ( 'cps-statistics' == $current_tab ) ? ' nav-tab-active' : '' ; ?>">
				<?php _e( 'Statistics', 'museums_network' ); ?>
			</a>
			<!-- /.nav-tab -->

			<!-- .nav-tab -->
			<a href="?page=content-provider-menu&tab=cps-connection" class="nav-tab<?php echo ( 'cps-connection' == $current_tab ) ? ' nav-tab-active' : '' ; ?>">
				<?php _e( 'Distributor Server', 'museums_network' ); ?>
			</a>
			<!-- /.nav-tab -->

		</h2>
		<!-- /.nav-tab-wrapper -->

		<?php 
			if ( 'cps-general' == $current_tab ) {
				cps_settings_panel();	
			} elseif ( 'cps-featured' == $current_tab ) {
				cps_featured_panel();
			} elseif ( 'cps-connection' == $current_tab ) {
				cps_connection_panel();	
				// Admin_Intefaces_Distributor::getInstance()->render();
			} elseif ( 'cps-statistics' == $current_tab ) {
				Admin_Interfaces_Statistics::getInstance()->render();
			}
		?>

	</div>
	<!-- /.wrap -->
<?php
}


function cps_settings_panel() { 
	global $wp_query, $wpdb; 

	if ( isset( $_POST['cps_action'] ) 
			&& ( 'featured_list' == $_POST['cps_action'] ) 
			&& wp_verify_nonce( $_POST['_wpnonce'] ) ) {

		update_option( 'cps_exhibition_zone_category'		,$_POST['cps_exhibition_zone_category'] );
		update_option( 'cps_items_category'							,$_POST['cps_items_category'] );
		update_option( 'cps_news_category'							,$_POST['cps_news_category'] );
		update_option( 'cps_events_category'						,$_POST['cps_events_category'] );
	}


	$available_categories = get_categories(array(
		'hide_empty'	=> 0
		,'type' 			=> 'post'
		,'orderby' 		=> 'id'
	))
?>

	<div class="clear"></div>
	<h2><?php _e( 'General Settings', 'museums_network' ); ?></h2>

	<form method="post">
		<input name="cps_action" type="hidden" value="featured_list" />
		<?php wp_nonce_field(); ?>
		<table class="form-table">
			<tbody>

				<!-- Exhibition Zones category -->
				<tr>
					<th>
						<?php 
							printf( '%s :', __( 'Exhibition Zones Category', 'museums_network' ) ); 
							$items_category = get_option( 'cps_exhibition_zone_category', 1 );
						?>
					</th>
					<td>
						<select id="cps_featured_category" name="cps_exhibition_zone_category">
							<?php foreach( $available_categories as $cat ) : ?>
								<option value="<?php echo $cat->term_id; ?>"<?php selected( $items_category, $cat->term_id ); ?>>
									<?php echo esc_html( $cat->name ); ?>
								</option>
							<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<!-- Items category -->

				<!-- Items category -->
				<tr>
					<th>
						<?php 
							printf( '%s :', __( 'Items Category', 'museums_network' ) ); 
							$items_category = get_option( 'cps_items_category', 1 );
						?>
					</th>
					<td>
						<div class="clearfix">
							<select id="cps_featured_category" name="cps_items_category">
								<?php foreach( $available_categories as $cat ) : ?>
									<option value="<?php echo $cat->term_id; ?>"<?php selected( $items_category, $cat->term_id ); ?>>
										<?php echo esc_html( $cat->name ); ?>
									</option>
								<?php endforeach; ?>
							</select>
						</div>
						<small>
							<?php printf( 
								'%s: %s.', 
								__( 'Recommended', 'museums_network' )
								, __( 'Items category should be child category of Exhibition Zone that you selected above' ) ); 
							?>
						</small>
					</td>
				</tr>
				<!-- Items category -->

				<!-- News category -->
				<tr>
					<th>
						<?php 
							printf( '%s :', __( 'News Category', 'museums_network' ) ); 
							$items_category = get_option( 'cps_news_category', 1 );
						?>
					</th>
					<td>
						<select id="cps_featured_category" name="cps_news_category">
							<?php foreach( $available_categories as $cat ) : ?>
								<option value="<?php echo $cat->term_id; ?>"<?php selected( $items_category, $cat->term_id ); ?>>
									<?php echo esc_html( $cat->name ); ?>
								</option>
							<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<!-- /News categoy -->

				<!-- Events category -->
				<tr>
					<th>
						<?php 
							printf( '%s :', __( 'Events Category', 'museums_network' ) ); 
							$items_category = get_option( 'cps_events_category', 1 );
						?>
					</th>
					<td>
						<select id="cps_featured_category" name="cps_events_category">
							<?php foreach( $available_categories as $cat ) : ?>
								<option value="<?php echo $cat->term_id; ?>"<?php selected( $items_category, $cat->term_id ); ?>>
									<?php echo esc_html( $cat->name ); ?>
								</option>
							<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<!-- /Events category -->

				<tr>
					<th>
						&nbsp;
					</th>
					<td>
						<input id="cps_update_featured_category" type="submit" class="button button-primary" value="<?php _e( 'Update Items Category', 'museums_network' ); ?>" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>

<?php
}

function cps_featured_panel() { ?>
	<h2><?php _e( 'Featured Items Settings', 'museums_network' ); ?></h2>
	<?php if ( isset( $_POST['cps_action'] ) && ( 'featured_list' == $_POST['cps_action'] ) ) cps_featured_panel_submit( $_POST ); ?>
	<form method="post">
		<input name="cps_action" type="hidden" value="featured_list" />
		<?php wp_nonce_field(); ?>
		<table class="form-table">
			<tbody>
				<tr>
					<th>
						<?php printf( '%s :', __( 'Featured Items Category', 'museums_network' ) ); ?>
					</th>
					<td>
						<?php 
							$cats = get_categories(array(
								'hide_empty' => 0
								,'type' => 'post'
							));

							$selected_featured_category = get_option( 'cps_featured_category', 1 );
						?>
						<select id="cps_featured_category" name="cps_featured_category">
							<?php foreach( $cats as $cat ) : ?>
								<option value="<?php echo $cat->term_id; ?>"<?php selected( $selected_featured_category, $cat->term_id ); ?>>
									<?php echo esc_html( $cat->name ); ?>
								</option>
							<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<th>
						&nbsp;
					</th>
					<td>
						<input id="cps_update_featured_category" type="submit" class="button button-primary" value="<?php _e( 'Update Featured Category', 'museums_network' ); ?>" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>

	<?php if ( is_admin() && get_option( 'cps_featured_category' ) ) : ?>

	<!-- .wrap -->
	<div class="wrap">
		<style type="text/css">
			#col_featured_id {width: 5%;}	
			#col_featured_title {width: 20%;}
			#col_featured_description {width: 50%;}
			#col_featured_author {width: 10%;}
			#col_featured_sync_status {width: 10%;}
		</style>
		<!-- #icon-users.icon32 -->
		<div id="icon-users" class="icon32"></div>
		<!-- /#icon-users.icon32 -->
		<h2><?php _e( 'Featured list', 'museums_network' ); ?></h2>

		<!-- #admin-featured-table-form -->
		<form id="admin-featured-table-form" action="" method="post">
			<?php 

				$featured_table = new Admin_Informations_FeaturedItemList(); 

				$page = filter_input( INPUT_GET, 'page', FILTER_SANITIZE_STRIPPED );
				$paged = filter_input( INPUT_GET, 'paged', FILTER_SANITIZE_NUMBER_INT );

				$featured_table->prepare_items();
				$featured_table->display();

			?>
		</form>
		<!-- /#admin-featured-table-form -->
	</div>
	<!-- /.wrap -->

	<?php endif; ?>

<?php }

function cps_connection_panel() { ?>

	<h2><?php _e( 'Update Interval Settings', 'museums_network' ); ?></h2>
	<?php 
		if ( isset( $_POST['cps_action'] ) && ( 'connection_setup' == $_POST['cps_action'] ) )
			cps_connection_setup( $_POST );
	?>
	<form method="post">
		<input name="cps_action" type="hidden" value="connection_setup" />
		<?php wp_nonce_field(); ?>
		<table class="form-table">
			<tbody>
				<!-- Sync Methodology -->
				<tr>
					<th>
						<?php printf( '%s :', __( 'Methodology of Synchronize', 'museums_network' ) ); ?>
					</th>
					<td>
						<?php $selected_method_of_sync = get_option( 'cps_method_of_sync', CPS_SYNC_METHOD_NOT_SYNC_DATA ); ?>
						<select id="cps_method_of_sync" name="cps_method_of_sync">

							<option value="<?php echo CPS_SYNC_METHOD_NOT_SYNC_DATA; ?>"<?php selected( $selected_method_of_sync, CPS_SYNC_METHOD_NOT_SYNC_DATA ); ?>>
								<?php _e( 'Not sync data', 'museums_network' ); ?>
							</option>

							<option value="<?php echo CPS_SYNC_METHOD_IMMEDIATELY_SYNC; ?>"<?php selected( $selected_method_of_sync, CPS_SYNC_METHOD_IMMEDIATELY_SYNC ); ?>>
								<?php _e( 'Immediately sync', 'museums_network' ); ?>
							</option>

							<option value="<?php echo CPS_SYNC_METHOD_HOURLY_SYNC; ?>"<?php selected( $selected_method_of_sync, CPS_SYNC_METHOD_HOURLY_SYNC ); ?>>
								<?php _e( 'Hourly', 'museums_network' ); ?>
							</option>

							<option value="<?php echo CPS_SYNC_METHOD_DAILY_SYNC; ?>"<?php selected( $selected_method_of_sync, CPS_SYNC_METHOD_DAILY_SYNC ); ?>>
								<?php _e( 'Daily', 'museums_network' ); ?>
							</option>

							<option value="<?php echo CPS_SYNC_METHOD_WEEKLY_SYNC; ?>"<?php selected( $selected_method_of_sync, CPS_SYNC_METHOD_WEEKLY_SYNC ); ?>>
								<?php _e( 'Weekly', 'museums_network' ); ?>
							</option>

							<option value="<?php echo CPS_SYNC_METHOD_MANUALLY_SYNC; ?>"<?php selected( $selected_method_of_sync, CPS_SYNC_METHOD_MANUALLY_SYNC ); ?>>
								<?php _e( 'Manually', 'museums_network' ); ?>
							</option>

							<option value="<?php echo CPS_SYNC_METHOD_CUSTOM_TIME_SYNC; ?>"<?php selected( $selected_method_of_sync, CPS_SYNC_METHOD_CUSTOM_TIME_SYNC ); ?>>
								<?php _e( 'Custom', 'museums_network' ); ?>
							</option>

						</select>
					</td>
				</tr>

				<!-- Submit section -->
					<tr>
						<th>&nbsp;</th>
						<td>
						<input id="submit-schedule" type="submit" class="button button-primary" value="<?php _e( 'Update Sync Condition', 'museums_network' ); ?>" />
						</td>
					</tr>
				<!-- /Submit section -->
			</tbody>
		</table>
	</form>

	<h2><?php _e( 'Distributor Server account', 'museums_network' ); ?></h2>
	<small class="cps-description clearfix">
		<?php printf(
			'%1$s <a href="%2$s/museum/regsiter" title="%2$s" target="_blank">%3$s</a> %4$s'
			, __( 'If you does not have an account, Please', 'museums_network' )
			, DISTRIBUTOR_SERVER_URL
			, __( 'click here', 'museums_network' )
			, __( 'for register', 'museums_network' )
		); ?>
	</small>

	<div class="clear"></div>

	<form enctype="multipart/form-data" method="post" action="" id="museum-data-pack">
		<?php wp_nonce_field(); ?>
		<input name="cps_action" type="hidden" value="ds_account" />
		<table class="form-table">
			<tbody>
				<?php $account = get_option( 'ds_account' ); ?>
				<!-- Account -->
				<tr>
					<th scope="row">
						<label for="cps_ds_username" class="clearfix">
							<?php printf( '%s :', __( 'Distributor Server Account', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
						<input id="cps_ds_username" name="cps_ds_username" value="<?php echo @$account['username']; ?>" type="text" class="clearfix" placeholder="<?php _e( 'Username', 'museums_network' ); ?>" />
					</td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td>
						<input id="cps_ds_password" name="cps_ds_username" value="<?php echo @$account['password']; ?>" type="password" placeholder="<?php _e( 'Password', 'museums_network' ); ?>" />
					</td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td>
						<button id="cps-update-ds-account" class="button button-primary alignleft" style="margin-right: 10px;"><?php _e( 'Update account', 'museums_network' ); ?></button>

						<button id="cps-test-connection" class="button alignleft" style="margin-right: 10px;">
							<?php _e( 'Test connection', 'museums_network' ); ?>
						</button>

						<div id="cps-test-connection-status" class="cps-loading alignleft hide"></div>

					</td>
				</tr>
				<!-- /account Status -->
			</tbody>
		</table>
	</form>

	<div class="clear"></div>

	<?php  
		if ( isset( $_POST['cps_action'] ) && ( 'register_museum' == $_POST['cps_action'] ) ) 
			add_register_record( $_POST ); 

		$museum_info = get_posts(array(
			'post_type' 			=> 'museum-info', 
            'post_status'		=> 'private',
		));

		$museum_info = ( ! empty( $museum_info ) ) ? $museum_info[0] : '' ;
		global $post;

		$post = $museum_info;
		setup_postdata( $post );

		$id = get_the_ID();

		if ( empty( $_POST ) && isset( $_GET['remove_post_thumbnail'] ) &&  'yes' == strtolower( $_GET['remove_post_thumbnail'] ) ) 
			wp_delete_attachment( get_post_thumbnail_id(), true ); 

	?>
	<h2><?php _e( 'Museum Registration form', 'museums_network' ); ?></h2>

	<!-- Museum data -->
	<form enctype="multipart/form-data" method="post" action="" id="museum-data-pack">
		<?php wp_nonce_field(); ?>
		<input name="cps_action" type="hidden" value="register_museum" />
		<table class="form-table">
			<tbody>

				<!-- Connection Status -->
				<tr>
					<th scope="row">
						<label for="connection_status">
							<?php printf( '%s :', __( 'Connection Status', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
						<span id="connection_status" class="clearfix">
							<?php
								isset( $connection_status ) || $connection_status = get_option( 'cps_connection_status', CPS_Distributor_Server_Connection::CPS_DS_CONNECTION_UNKNOW_CONNECTION );

								echo CPS_Distributor_Server_Connection::localized_server_connection_message( $connection_status );
							?>
						</span>

						<small class="clearfix">
							<span class="cps_tools">
								<a id="cps_connection_test" href="#" title="<?php _e( 'Check server connection', 'museums_network' ); ?>">
									<?php _e( 'Check server connection', 'museums_network' ); ?>
								</a>
							</span>
						</small>
					</td>
				</tr>
				<!-- /Connection Status -->

				<?php if ( $ds_id = get_option( 'ds_museum_id', 0 ) ) : ?>
					<!-- Museum ID from DS -->
					<tr class="form-field form-required">
						<th scope="row">
							<label>
								<?php printf( '%s : ', __( 'Museum ID', 'museums_network' ) ); ?>
							</label>
						</th>
						<td>
							<?php echo $ds_id; ?>
						</td>
					</tr>
					<!-- /Museum ID from DS -->
				<?php endif; ?>
				
				<!-- Meseum's name -->
				<tr class="form-field form-required">
					<th scope="row">
						<label for="museum_title">
							<?php printf( '%s : ', __( 'Museum name', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
						<input id="museum_title" name="museum_title" type="text" placeholder="<?php _e( 'Museum name', 'museums_network' ); ?>" value="<?php echo ( @$museum_info->post_title ) ? $museum_info->post_title : get_bloginfo( 'name' ) ;  ?>" />
					</td>
				</tr>
				
				<!-- Meseum's name English -->
				<tr class="form-field form-required">
					<th scope="row">
						<label for="museum_title_english">
							<?php printf( '%s : ', __( 'Museum name in English', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
						<input id="museum_title_english" name="museum_title_english" type="text" placeholder="<?php _e( 'Museum name in English', 'museums_network' ); ?>" value="<?php echo ( $title_eng = get_post_meta( @$museum_info->ID, 'museum_title_english', true ) ) ? $title_eng : '' ; // @$museum_info->post_title ) ? $museum_info->post_title : get_bloginfo( 'name' ) ;  ?>" />
					</td>
				</tr>

				<!-- Description -->
				<tr class="form-field form-required">
					<th scope="row">
						<label for="museum_description">
							<?php printf( '%s : ', __( 'Museum Introduction', 'museums_network' ) ); ?>
						</label>
					</th>
					<td class="cps_museum_description_class">
						<?php
							wp_editor(
								@$museum_info->post_content
								, 'museum_description'
								, array(
									'quicktags' 			=> false	
									, 'media_buttons' => false
								)
							);
						?>
					</td>
				</tr>

				<!-- Try Feature Image -->
				<?php if ( has_post_thumbnail() ) : ?>

					<tr class="form-field form-required">
						<th scope="row" rowspan="2">
							<label for="museum_description">
								<?php printf( '%s : ', __( 'Museum Featured Image', 'museums_network' ) ); ?>
							</label>
						</th>
						<td> <?php
							$post_thumbnail_url = wp_get_attachment_image_src( 
								get_post_thumbnail_id()
								, 'thumbnail' );

							$original_post_thumbnail_url = wp_get_attachment_image_src( 
								get_post_thumbnail_id()
								, 'large'  );

							printf( 
								'<a href="%s" target="_blank" class="thickbox"><img src="%s" width="%s" height="%s" title="%s" alt="%s" /></a>'
								,$original_post_thumbnail_url[0]
								,$post_thumbnail_url[0]
								,$post_thumbnail_url[1]
								,$post_thumbnail_url[2]
								,get_the_title()
								,get_the_title() ); ?>
						</td>
					</tr>

					<tr>
						<td>
							<a id="remove-featured-image" href="<?php echo $_SERVER['REQUEST_URI']; ?>&remove_post_thumbnail=yes" 
								title="<?php _e( "This picture will be delete permanently from server.\n Do you want to delete ?", 'museums_network' ) ?>"
								class="button primary-button" 
								data-confirm="<?php _e( "This picture will be delete permanently from server.\n Do you want to delete ?", 'museums_network' ) ?>">
									<?php _e( 'Remove post thumbnail', 'museums_network' ); ?>
							</a>
						</td>

					</tr>

				<?php else: ?>
					<tr class="form-field form-required">
						<th scope="row">
							<label for="museum_description">
								<?php printf( '%s : ', __( 'Museum Featured Image', 'museums_network' ) ); ?>
							</label>
						</th>
						<td>
							<input name="museum_featured_image" type="file" />
						</td>
					</tr>
				<?php endif; ?>
				<!-- /Image -->

				<!-- Location -->
				<tr class="form-field form-required">
					<th scope="row">
						<label for="museum_location">
							<?php printf( '%s : ', __( 'Location', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
						<?php 
							$id = @$museum_info->ID;
							$lat = ( $l = get_post_meta( $id, 'latitude', true ) ) ? $l : '13.75' ;
							$lng = ( $l = get_post_meta( $id, 'longitude', true ) ) ? $l : '100.466667' ;
						?>
						<div id="museums_map_canvas" class="museums_location" data-latitude="<?php echo @$lat; ?>" data-longitude="<?php echo @$lng; ?>"></div>

						<div class="clear"></div>

						<div class="alignleft actions cps-locations" id="current-location">
							<span class="location-label">
								<a id="cps-get-current-location" href="#current-lcoation" title="<?php _e( 'Get current location', 'museums_network' ); ?>">
									<?php _e( 'Get current location', 'museums_network' ); ?>
								</a>
							</span>

							<div id="get-current-location" class="cps-loading hide"></div>
						</div>

						<div class="alignright actions cps-locations">
							<span class="location-label">
								<?php printf( '%s :', __( 'Latitude', 'museums_network' ) ); ?>
							</span>
							<input id="museum_location_lat" placeholder="0.0" name="museum_location[latitude]" readonly="readonly" type="text" value="<?php echo $lat; ?>" />
						</div>

						<div class="alignright actions cps-locations">
							<span class="location-label">
								<?php printf( '%s :', __( 'Longitude', 'museums_network' ) ); ?>
							</span>
								<input id="museum_location_lng" placeholder="0.0" name="museum_location[longitude]" readonly="readonly" type="text" value="<?php echo $lng; ?>" />
						</div>

					</td>
				</tr>

				<!-- Museum Description -->
				<tr class="form-field form-required">
					<th scope="tow">
						<label for="museum_description">
							<?php printf( '%s :', __( 'Museum description' ) ); ?>
						</label>
					</th>
					<td>
						<div class="">
							<input id="museum_location_desc" placeholder="<?php _e( 'Location description', 'museums_network' ); ?>" value="<?php echo get_post_meta( $id, 'location_description', true ); ?>" name="museum_location[location_description]" type="text" class="widefat" />
						</div>
						<small><?php _e( 'Add more description for location here', 'museums_network' ); ?></small>
					</td>
				</tr>

				<!-- Website -->
				<tr class="form-field form-required">
					<th scope="row">
						<label for="museum_location">
							<?php printf( '%s : ', __( 'Official web site', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
						<input placeholder="<?php _e( 'Office web site', 'museums_network' ); ?>" name="official_website" type="text" class="widefat" value="<?php echo ( $tmp = get_post_meta( $id, 'official_website', true ) ) ? $tmp : get_bloginfo( 'url' ) ; ?>" />
					</td>
				</tr>

				<!-- Content Provider Site -->
				<tr class="form-field form-required">
					<th scope="row">
						<label for="museum_content_provider">
							<?php printf( '%s : ', __( 'Content Provider site', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
						<input id="museum_content_provider" placeholder="<?php _e( 'Content Provider site', 'museums_network' ); ?>" name="museum_content_provider" type="text" class="widefat clear" value="<?php echo ( $tmp = get_post_meta( $id, 'museum_content_provider', true ) ) ? $tmp : get_bloginfo( 'url' ) . '/xmlrpc.php' ; ?>" />
						<small><?php _e( 'Distributor will not publish this url', 'museums_network' ); ?></small>
					</td>
				</tr>

				<!-- Museum connection -->
				<tr class="form-field form-required">
					<th scope="row">
						<label for="museum_connection">
							<?php printf( '%s : ', __( 'Museum connection', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
					<input id="museum_connection" placeholder="<?php _e( 'Museum connection information', 'museums_network' ); ?>" name="museum_connection" type="text" class="widefat clear" value="<?php echo ( $tmp = get_post_meta( $id, 'museum_connection', true ) ) ? $tmp : '' ; ?>" />
						<small><?php _e( 'Telephone number, Fax sparate with comma (",")', 'museums_network' ); ?></small>
					</td>
				</tr>

				<!-- E-mail -->
				<tr class="form-field form-required">
					<th scope="row">
						<label for="museum_email">
							<?php printf( '%s : ', __( 'E-mail', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
					<input id="museum_email" placeholder="<?php _e( 'E-mail', 'museums_network' ); ?>" name="museum_email" type="text" class="widefat clear" value="<?php echo ( $tmp = get_post_meta( $id, 'museum_email', true ) ) ? $tmp : '' ; ?>" />
					</td>
				</tr>

				<!-- SSID -->
				<tr class="form-field form-required">
					<th scope="row">
						<label for="ssid_list">
							<?php printf( '%s : ', __( 'SSID', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
                        <input id="wifi-ssid" placeholder="<?php _e( 'SSID', 'museums_network' ); ?>" name="ssid_list" type="text" class="widefat clear" value="<?php echo ( $tmp = get_post_meta( $id, 'ssid_list', true ) ) ? $tmp : '' ; ?>" />
                        <br />
						<small><?php _e( 'Wi-Fi access point name, separate each one with comma (",")', 'museums_network' ); ?></small>
					</td>
				</tr>

				<!-- Open dates -->
				<tr class="form-field form-required">
					<th scope="row">
						<label for="museum_date">
							<?php printf( '%s : ', __( 'Open date', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
						<style type="text/css">
							/** Hacking WordPress style **/	
							.date-time input[type=checkbox] { width: auto !important; }j
						</style>
						<table class="form-table date-time">
							<thead>
								<tr>
									<th><?php _e( 'Day', 'museums_network' ); ?></th>
									<th><?php _e( 'Open', 'museums_network' ); ?></th>
									<th><?php _e( 'From', 'museums_network' ); ?></th>
									<th><?php _e( 'Until', 'museums_network' ); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$days = array(
												'sun' => __( 'Sunday', 'museums_network' ), 
												'mon' => __( 'Monday', 'museums_network' ), 
												'tue' => __( 'Tuesday', 'museums_network' ), 
												'wed' => __( 'Wednesday', 'museums_network' ), 
												'thu' => __( 'Thursday', 'museums_network' ), 
												'fri' => __( 'Friday', 'museums_network' ), 
												'sat' => __( 'Saturday', 'museums_network' )
											);

									$servicetime = get_post_meta( $id, 'cps_servicetime', true );

									foreach( $days as $dayshort => $day ) : ?>
										<tr>
											<td><?php echo $day; ?></td>

											<td>
												<input id="service-status-<?php echo $dayshort; ?>" 
													name="servicetime[<?php echo $dayshort; ?>][status]" 
													type="checkbox" 
													value="open"
													<?php checked( @$servicetime[$dayshort]['status'], 'open' ); ?>/>
											</td>

											<td>
												<select id="" name="servicetime[<?php echo $dayshort; ?>][open_time]">
													<?php 
														$hour = 0; $minute = 0;
														while( $hour <= 24 ) { ?>
															<?php $selected_time = ( isset( $servicetime[ $dayshort ] ) && isset( $servicetime[ $dayshort ]['open_time'] ) ) ? $servicetime[ $dayshort ]['open_time'] : '' ; ?>
														<option value="<?php printf( '%1$02d%2$02d', $hour, $minute ); ?>"<?php selected( $selected_time, sprintf( '%1$02d%2$02d', $hour, $minute ) ); ?>>
																<?php printf( '%1$02d:%2$02d', $hour, $minute ); ?>
															</option>
															<?php
															if ( 60 == ( $minute += 15 ) ) {
																$minute = 0;
																$hour++;
															}
														}
													?>
												</select>
											</td>
											<td>
												<select id="" name="servicetime[<?php echo $dayshort; ?>][close_time]">
													<?php 
														$hour = 0; $minute = 0;
														while( $hour <= 24 ) { ?>
															<?php $selected_time = ( isset( $servicetime[ $dayshort ] ) && isset( $servicetime[ $dayshort ]['close_time'] ) ) ? $servicetime[ $dayshort ]['close_time'] : '' ; ?>
															<option value="<?php printf( '%1$02d%2$02d', $hour, $minute ); ?>"<?php selected( $selected_time, sprintf( '%1$02d%2$02d', $hour, $minute ) ); ?>>
																<?php printf( '%1$02d:%2$02d', $hour, $minute ); ?>
															</option>
															<?php
															if ( 60 == ( $minute += 15 ) ) {
																$minute = 0;
																$hour++;
															}
														}
													?>
												</select>
											</td>
										</tr>
									<?php endforeach; ?>
							</tbody>
						</table>
					</td>
				</tr>

				<!-- Museum Status -->
				<tr class="form-field form-required">
					<th scope="row">
						<label for="museum_status">
							<?php printf( '%s : ', __( 'Musuem status', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
						<?php $museum_status = get_post_meta( $id, 'cps_museum_status', true ); ?>
						<select id="museum_status" name="museum_status">
							<option value="open"<?php selected( $museum_status, 'open' ); ?>>
								<?php _e( 'Open', 'museums_network' ); ?>
							</option>
							<option value="closed"<?php selected( $museum_status, 'closed' ); ?>>
								<?php _e( 'Closed', 'museums_network' ); ?>
							</option>
							<option value="close-for-maintenance"<?php selected( $museum_status, 'close-for-maintenance' ); ?>>
								<?php _e( 'Close for Maintenance', 'museums_network' ); ?>
							</option>
						</select>
					</td>
				</tr>

				<!-- Museum Type -->
				<tr class="form-field form-required">
					<th scope="row">
						<label for="museum_group">
							<?php printf( '%s : ', __( 'Museum type', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
                        <?php $museum_type = get_post_meta( $id, 'museum_type', true ); ?>
						<select id="cps_museum_type" name="cps_museum_type">
							<option value="public-museum"<?php selected( $museum_type, 'public-museum' ); ?>><?php _e( 'Public museum', 'museums_network' ); ?></option>
							<option value="private-museum"<?php selected( $museum_type, 'private-museum' ); ?>><?php _e( 'Private museum', 'museums_network' ); ?></option>
						</select>
					</td>
				</tr>

				<!-- Exhibition Type -->
				<tr class="form-field form-required">
					<th scope="row">
						<label for="museum_type">
							<?php printf( '%s : ', __( 'Exhibition type', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
						<?php $exhibition_type = get_post_meta( $id, 'exhibition_type', true ); ?>
						<select id="exhibition_type" name="exhibition_type">
							<option value="walk-in-museum"<?php selected( $exhibition_type, 'walk-in-museum' ); ?>>
								<?php _e( 'Walk-in Museum', 'museums_network' ); ?>
							</option>
							<option value="e-museum"<?php selected( $exhibition_type, 'e-museum' ); ?>>
								<?php _e( 'Electronic Museum', 'museums_network' ); ?>
							</option>
						</select> <br />
                        <small><?php _e( 'Please selecte exhibition type that is the best describe your exhibition', 'museums_network' ); ?></small>
					</td>
				</tr>

				<!-- Museum Groups -->
				<tr class="form-field form-required">
					<th scope="row">
						<label for="museum_group">
							<?php printf( '%s : ', __( 'Museum group', 'museums_network' ) ); ?>
						</label>
					</th>
					<td>
                        <?php 
                            $museum_category_id = get_post_meta( $id, 'cps_museum_category_selected', true ); 
                            $museum_category_list = cps_get_museum_groups();

                            if ( is_array($museum_category_list) ) : ?>
                                <select id="cps_museum_category_selected" name="cps_museum_category_selected">
                                    <?php foreach( $museum_category_list as $group): ?>
                                    <option value="<?php echo $group['group_id']; ?>"<?php selected( $museum_category_id, $group['group_id'] ) ?>>
                                        <?php echo $group['description']; ?> 
                                    </option>
                                    <?php endforeach; ?>
                                </select> <br />
                            <?php else : ?>
                                <?php _e( 'Can not connect with server', 'museums_network' ); ?>
                            <?php endif; ?>

                        <small><?php _e( 'Please select category that can describe your museum', 'museums_network' ); ?></small>
					</td>
				</tr>

				<!-- Submit -->
				<tr class="form-field form-required">
					<th scope="row">
						&nbsp;
					</th>
					<td class="submit-field">
						<?php
							$submit_message = get_option( 'cps_is_register_status', CPS_Registration_Status::CPS_REGISTRATION_NOT_REGISTER_STATUS );
							switch( $submit_message ) {
								case CPS_Registration_Status::CPS_REGISTRATION_NOT_REGISTER_STATUS:
									$submit_message = __( 'Register', 'museums_network' );

									$wp_salt = get_option( 'cps_connection_salt', wp_salt( 'nonce' ) );
									$pub_key = hash( 'sha256', $wp_salt, false );
									$pri_key = hash_hmac( 'sha256', $pub_key, $wp_salt, false );

									update_option( 'cps_connection_salt', $wp_salt );
									update_option( 'cps_connection_pub_key', $pub_key );
									update_option( 'cps_connection_pri_key', $pri_key );

									break;

								default:
									$submit_message = __( 'Update', 'museums_network' );
									break;

							}
						?>
						<input class="button button-primary" style="width:auto;" type="submit" value="<?php echo $submit_message; ?>" />
						&nbsp;
						
					</td>

				</tr>

			</tbody>
		</table>
		
	
	</form>

<?php
}

add_action( 'cps_admin_scripts', 'cps_admin_scripts' );
function cps_admin_scripts() {

	wp_enqueue_script(
		'cps-admin-script'
		, plugin_dir_url( __FILE__ ) . 'assets/js/cps-admin-script.js' 
		, array('jquery', 'google-maps-api')
		, '0.1.0'
		, true
	);

	wp_enqueue_script(
		'google-maps-api' 
		, 'https://maps.googleapis.com/maps/api/js?sensor=true&lang=th'
		, array()
		, '0.1.0'
		, true
	);
}

add_action( 'cps_admin_styles', 'cps_admin_styles' );
function cps_admin_styles() {
	if ( ! is_admin() ) return;

	wp_enqueue_style(
		'cps-custom-admin-wp-hook'
		, admin_url( 'load-styles.php?c=1&dir=ltr&load=admin-bar,wp-admin,buttons' )
	);
	
	wp_enqueue_style(
		'cps-admin-style'
		, plugin_dir_url( __FILE__ ) . 'assets/css/cps-admin-style.css' 
		, array( 'cps-custom-admin-wp-hook' )
		, '0.1.0'
		, 'screen'
	);
}

function cps_connection_setup( $post_params ) {
	$request_url = $_SERVER['REQUEST_URI'];
	if ( ! wp_verify_nonce( $post_params['_wpnonce'] ) || ( $_SERVER['REQUEST_URI'] != $post_params['_wp_http_referer'] ) )
		return;

	if ( is_numeric( $post_params['cps_method_of_sync'] ) ) 
		update_option( 'cps_method_of_sync', $post_params['cps_method_of_sync'] );
	
	update_option( 'cps_sync_result_counting', array() );

    
    wp_schedule_single_event( time() + 5, 'cps_update_synchronize_item_method' );

}

function add_register_record( $post_params ) {

	$request_url = $_SERVER['REQUEST_URI'];
	if ( ! wp_verify_nonce( $post_params['_wpnonce'] ) || ( $_SERVER['REQUEST_URI'] != $post_params['_wp_http_referer'] ) )
		return;

	$museum_post_params = array();

    // If Museum already exists function will get update
    if ( get_option( 'ds_local_museum_id', false ) ) $museum_post_params['ID'] = get_option( 'ds_local_museum_id' ) ;

	$current_user = wp_get_current_user();

	$date_gmt = date('Y-m-d H:i:s');
	$date = get_date_from_gmt( $date_gmt );

	$museum_post_params['comment_status'] = 'closed';
	$museum_post_params['ping_status'] = 'closed';
	$museum_post_params['post_author'] = $current_user->ID;
	$museum_post_params['post_content'] = $post_params['museum_description'];
	$museum_post_params['post_title'] = $post_params['museum_title'];
	$museum_post_params['post_type'] = 'museum-info';
	$museum_post_params['post_date'] = $date;
	$museum_post_params['post_date_gmt'] = $date_gmt;
	$museum_post_params['post_status'] = 'private';

	$local_museum_id = wp_insert_post( $museum_post_params );
    update_option( 'ds_local_museum_id', $local_museum_id );

	if ( is_wp_error( $local_museum_id ) ) return;

    // English name for this museum
	update_post_meta( $local_museum_id, 'museum_title_english', $post_params['museum_title_english']);

    // Location information
    // -- Description
	update_post_meta( $local_museum_id, 'location_description', $post_params['museum_location']['location_description'] );
    // -- Latitude 
	update_post_meta( $local_museum_id, 'latitude', $post_params['museum_location']['latitude'], get_post_meta( $local_museum_id, 'latitude', true ) );
    // -- Longitude 
	update_post_meta( $local_museum_id, 'longitude', $post_params['museum_location']['longitude'], get_post_meta( $local_museum_id, 'longitude', true ) );

    // URL for connetion by user and application connection
    // -- Public website
	update_post_meta( $local_museum_id, 'official_website', $post_params['official_website']);
    // -- Conten provider Location
	update_post_meta( $local_museum_id, 'museum_connection', $post_params['museum_connection'], get_post_meta( $local_museum_id, 'museum_connection', true ) );

	update_post_meta( $local_museum_id, 'museum_email', $post_params['museum_email'], get_post_meta( $local_museum_id, 'museum_email', true ) );
	update_post_meta( $local_museum_id, 'cps_exhibition_status', $post_params['exihibition_status']);
	update_post_meta( $local_museum_id, 'museum_status', $post_params['museum_status'], get_post_meta( $local_museum_id, 'museum_status', true ) );
    update_post_meta( $local_museum_id, 'exhibition_type', $post_params['exhibition_type'], get_post_meta( $local_museum_id, 'exhibition_type', true ) );

	// Museum type 
	update_post_meta( $local_museum_id, 'museum_type', $post_params['cps_museum_type'], 'public-museum' );

    // Museum category this category sync from server
    update_post_meta( $local_museum_id, 'selected_museum_category', $post_params['cps_museum_category_selected'] );

    // Mask this topic should be sync
	update_post_meta( $local_museum_id, 'cps_sync_required', 'yes', get_post_meta( $local_museum_id, 'cps_sync_required', true ) );

    // SSID
    update_post_meta( $local_museum_id, 'ssid_list', $post_params['ssid_list'] );

	if ( ! empty( $_FILES ) ) {

		if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );

		$featured_attached_image = $_FILES['museum_featured_image'];
		$featured_attached_image = wp_handle_upload( $featured_attached_image, array( 'test_form' => false ) );

		if ( ! array_key_exists( 'error', $featured_attached_image ) ) {
			// $attached_id = wp_insert_post( $featured_attached_image );
			$wp_upload_dir = wp_upload_dir();

			$attachment = array(
				'guid' 				=> $featured_attached_image['url']
				,'post_mime_type'	=> $featured_attached_image['type']
				,'post_title'		=> $museum_post_params['post_title']
				,'post_content'		=> $museum_post_params['post_content']
				,'post_status'		=> 'inherit'
			);

			$attached_id = wp_insert_attachment( 
				$attachment
				,$featured_attached_image['file']
				,$local_museum_id 
			);

			require_once ABSPATH . 'wp-admin/includes/image.php';
			$attach_data = wp_generate_attachment_metadata( $attached_id, $featured_attached_image['file'] );
			wp_update_attachment_metadata( $attached_id, $attach_data );

			set_post_thumbnail( $local_museum_id, $attached_id );
		}

	}

	do_action( 'start_sync_meseum_data', $local_museum_id );
	// cps_schedule_sync_museum_data( $local_museum_id );
}

function cps_featured_panel_submit( $post_params ) {
	$request_url = $_SERVER['REQUEST_URI'];
	if ( ! wp_verify_nonce( $post_params['_wpnonce'] ) || ( $_SERVER['REQUEST_URI'] != $post_params['_wp_http_referer'] ) )
		return;
	
	update_option( 'cps_featured_category' ,$_POST['cps_featured_category'] );
}

add_action( 'start_sync_meseum_data', 'cps_sync_museum_data', 5, 1 );
function cps_sync_museum_data( $museum_id ) {
	update_option( 
		'cps_connection_status', 
		CPS_Distributor_Server_Connection::CPS_DS_CONNECTION_SYNC_IN_PROGRESS 
	);

    // wp_schedule_single_event( 
    //     time() + 5 , 
    //     'schedule_sync_museum_data' , 
    //     array( 'meseum_id' => $museum_id ) 
    // );
    // cps_schedule_sync_museum_data( $museum_id );
    //

    do_action( 'schedule_sync_museum_data', $museum_id );
}

add_action( 'update_open_date_time', 'cps_update_open_date_time', 10, 1 );
function cps_update_open_date_time( $museum_id = 0 ) {
	if ( class_exists( 'IXR_Client' ) ) {
		require_once ABSPATH . WPINC . '/class-IXR.php';
		require_once ABSPATH . WPINC . '/class-wp-http-ixr-client.php';
	}

	$client = new IXR_Client( DISTRIBUTOR_SERVER );

	$account = get_option( 'ds_account' );
	$inter_museum_id = get_option( 'ds_museum_id' );

	if ( ! $account || ! $inter_museum_id ) return;

	$date_payload = array();
	$date_payload[] = $account['username'];
	$date_payload[] = $account['password'];
	$date_payload[] = $inter_museum_id;

	$open_date_time = get_post_meta( $museum_id, 'cps_servicetime', true );
	$date_order = array( 'sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat' );

	foreach( $date_order as $date_name ) {
		$date_info = $open_date_time[$date_name];
		// Open or Close
		$date_payload[] = isset( $date_info['status'] ) ? $date_info['status'] : 'close' ;
		$date_payload[] = isset( $date_info['open_time'] ) ? $date_info['open_time'] : '0000' ;
		$date_payload[] = isset( $date_info['close_time'] ) ? $date_info['close_time'] : '0000' ;
	}

	$client->query(
		'mp.updateMuseumOpenDate',
		$date_payload
	);

}

add_action( 'schedule_sync_museum_data', 'cps_schedule_sync_museum_data', 5, 1 );
function cps_schedule_sync_museum_data( $museum_id ) {
	require_once ABSPATH . WPINC . '/class-IXR.php';
	require_once ABSPATH . WPINC . '/class-wp-http-ixr-client.php';

	$client = new IXR_Client( DISTRIBUTOR_SERVER );
    $client->debug = true;

	wp_reset_postdata();

    // Update museum information
	$client->query(
		'mp.updateMuseumInformation', 
        ds_museum_payload_formatted( $museum_id )        
	);

	$resp = $client->getResponse();

	if ( 'success' == $resp['status'] ) {
		update_option( 'ds_museum_id', $resp['message']['museum_id'] );

		update_option( 
			'cps_connection_status', 
			CPS_Distributor_Server_Connection::CPS_DS_CONNECTION_CONNECTED 
		);

		do_action( 'update_open_date_time', $museum_id );

	} else {
		update_option( 
			'cps_connection_status', 
			CPS_Distributor_Server_Connection::CPS_DS_CONNECTION_ERROR_WHILE_CONNECTION 
		);

	}

	wp_reset_postdata();
	wp_reset_query();

	update_option( 'cps_is_register_status', CPS_Registration_Status::CPS_REGISTRATION_PENDING_FOR_APPROVAL_STATUS  );
}

// Callback function 
add_action( 'cps_update_synchronize_item_method', 'cps_update_synchronize_item_method' );
function cps_update_synchronize_item_method() {
}

function cps_get_museum_types() {
    do_action('cps_get_museum_types');
}

function cps_get_museum_groups() {
    do_action('cps_get_museum_groups');

    if ( get_option('cps_available_groups') ) 
        return get_option('cps_available_groups');

    do_action('cps_update_museums_groups_daily');

    return get_option('cps_available_groups');
}

/**
 * Museum groups daily sync
 **/
add_action('wp', 'cps_schedule_sync' );
function cps_schedule_sync() {
    if ( ! wp_next_scheduled( 'cps_update_museums_groups_daily' ) ) 
        wp_schedule_event( time(), 'daily', 'cps_update_museums_groups_daily' );

}

add_action( 'cps_update_museums_groups_daily', 'cps_update_museums_groups_daily' );
function cps_update_museums_groups_daily() {
    $client = new IXR_Client(DISTRIBUTOR_SERVER);

    $client->query('np.museum.group');

    $response = $client->getResponse();

    if (isset($response['groups'] )) 
        update_option('cps_available_groups', $response['groups']);
    
}

function cps_admin_update_museum_thumbnail( $post ) {
    $image_id = -1;

    if ( ! get_option( 'museum_image_need_update', true ) ) {

        $post_id = is_object( $post ) ? $post->ID : $post ;

        $image_file = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'large' );
        $image_url = $image_file[0];

        $upload_path = wp_upload_dir();
        $image_path = str_replace( $upload_path['baseurl'], $upload_path['basedir'], $image_url );
        
        $image_type = wp_check_filetype( $image_path );

        $image = fopen( $image_path, 'r' );
        $image_content = fread( $image, filesize( $image_path ) );
        fclose( $image );

        $image_name = explode( DIRECTORY_SEPARATOR, $image_path );
        $image_name = array_pop( $image_name );

        $featured_image_params = array(
            'name'          => $image_name
            ,'type'         => $image_type['type']
            , 'bits'        => new IXR_Base64( $image_content )
            , 'overwrite'   => false
        );

        $ds_account = get_option( 'ds_account' );

        $client->query(
            'wp.uploadFile', 
            1, 
            $ds_account['username'], 
            $ds_account['password'], 
            $featured_image_params
        );

        if ( ! $client->isError() ) {

            $uploadImageResponse = $client->getResponse();
            $content_payload[] = $uploadImageResponse['id'];

            update_option( 'museum_image_need_update', false );
            update_option( 'museum_thumbnail_image_id', $uploadImageResponse['id'] );

            $image_id = $uploadImageResponse['id'];
        }

    } else {
        $image_id = get_option( 'museum_thumbnail_image_id' );
    }

    return $image_id;
}

function ds_museum_payload_formatted( $museum_id ) {
    $image_id = cps_admin_update_museum_thumbnail( $museum_info->ID );

	$museum_id = is_array( $museum_id ) ? $museum_id['museum_id'] : $museum_id ;
	$museum_info = get_post( $museum_id );

	$ds_account = get_option( 'ds_account', '' );


    $museum_id = get_option( 'ds_museum_id', -1 );
	$museum_id = empty( $museum_id ) ? -1 : $museum_id ;

    $museum_payload = array();
	// User's credential for connect with Distributor server
    $museum_payload[] = $ds_account['username'];
    $museum_payload[] = $ds_account['password'];

	// 2: Museum id (if exists)
	$museum_payload[] = $museum_id;

	// 3: Museum Title
	$museum_payload[] = $museum_info->post_title;

	// 4: Museum id (if exists)
	$museum_payload[] = get_post_meta( $museum_info->ID, 'museum_title_english', true );

	// 5: Museum description 
	$museum_payload[] = strip_tags( $museum_info->post_content );

	// 6: Location description 
	$museum_payload[] = get_post_meta( $museum_info->ID, 'location_description', true );

	// 7: Location latitude
	$museum_payload[] = get_post_meta( $museum_info->ID, 'latitude', true );

	// 8: Location longitude 
	$museum_payload[] = get_post_meta( $museum_info->ID, 'longitude', true );

	// 9: E-mail 
	$museum_payload[] = get_post_meta( $museum_info->ID, 'museum_email', true );

	// 10: Status 
	$museum_payload[] = get_post_meta( $museum_info->ID, 'cps_museum_status', true );

	// 11: Contact 
	$museum_payload[] = get_post_meta( $museum_info->ID, 'museum_connection', true );

	// 12: Preferred museum category 
	$museum_payload[] = get_post_meta( $museum_info->ID, 'selected_museum_category', true );

	// 13: Official website 
	$museum_payload[] = get_post_meta( $museum_info->ID, 'official_website', true );

	// 14: Private API Connection 
	$museum_payload[] = get_post_meta( $museum_info->ID, 'museum_content_provider', true );

	// 15: Museum type 
	$museum_payload[] = get_post_meta( $museum_info->ID, 'museum_type', true );

	// 16: Exhibition type 
	$museum_payload[] = get_post_meta( $museum_info->ID, 'exhibition_type', true );

	// 17: Thumbnail Id
	$museum_payload[] = $image_id;

    // 18: SSID
    $museum_payload[] = get_post_meta( $museum_info->ID, 'ssid_list', true );

	return $museum_payload;
}
