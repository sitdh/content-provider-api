<?php
require_once ( 'statistics-init.php' );
register_activation_hook( '../content-provider.php', array( 'SLStatistics', 'statistics_preparation' ) );

function sl_update_user_statistics( $user_info, $checkin_place_id, $item_id = null, $item_name = null, $checkin_timestamp = SLStatics::STATISTICS_NONE_AVAILABLE_VALUE ) {
    global $statistics;

    return $statistics->statistics_update_user_statistics( 
        $user_info, 
        $checkin_place_id,
        $item_id,
        $item_name,
        $checkin_timestamp
    );
}
