<?php

class SLStatistics {

    const STATISTICS_NONE_AVAILABLE_VALUE = -1;

    const STATISTICS_COUNT_CHECKIN_STATUS                   = 1;
    const STATISTICS_COUNT_FETCH_ITEM_INFORMATION_STATUS    = 2;

    const STATISTICS_ERROR_LOCALIZED_KEY                    = 'errorLocalizedStringKey';
    const STATISTICS_ERROR_USER_SEND_INFORMATION            = 'errorUserSendInformation';
    const STATISTICS_UPDATE_MESSAGE_RESULT_KEY              = 'updateMessageResultKey';
    const STATUSTICS_UPDATE_MESSAGE_INFORMATION_KEY         = 'updateMessageInformationKey'; 
    const STATISTICS_PARAM_ATTRIBUTE_ITEM_ID                = 'paramerterAttributeItemId';
    const STATISTICS_PARAM_ATTRIBUTE_ITEM_NAME              = 'paramerterAttributeItemName';

    const STATISTICS_ERROR_CODE_WRONG_PARAMETER_ASSINGED    = 100;

    const STATISTICS_ERROR_STRING_WRONG_PARAMETER_ASSINGED  = "Wrong parameter assinged or missing type";

    public function __construct() {
        global $wpdb;

        $wpdb->statistics = $wpdb->prefix . 'statistics';
    }

    public static function getInstance() {
        static $_statInstance = null;

        if ( null == $_statInstance ) {
            $_statInstance = new SLStatistics();
        }

        return $_statInstance;
    }

    public function statistics_table_prefix_register() {
        global $wpdb;

        $wpdb->statistics = $wpdb->prefix . 'statistics';
    }

    static function statistics_preparation() {
        global $wpdb;

        $wpdb->statistics = $wpdb->prefix . 'statistics';

        $query = 'CREATE TABLE IF NOT EXISTS ' 
            . $wpdb->statistics 
            . '(' 
            . 'statistics_id BIGINT AUTO_INCREMENT, '
            . 'user_id BIGINT NOT NULL, '
            . 'place_id BIGINT NOT NULL, '
            . 'item_id BIGINT, '
            . 'item_name VARCHAR(250), '
            . 'statistics_type INT(1), '
            . 'checked_in_timestamp_gmt TIMESTAMP DEFAULT CURRENT_TIMESTAMP, '
            . 'PRIMARY KEY (statistics_id)) '
            . 'CHARACTER SET utf8 COLLATE utf8_general_ci';

        $wpdb->query( $query );
    }

    public function statistics_update_user_statistics( $user, $place_id, $item_id = null, $item_name = null, $checkin_timestamp = self::STATISTICS_NONE_AVAILABLE_VALUE ) {
        global $wpdb;

        $stat_record = array();

        $stat_record[ 'statistics_type' ] = self::STATISTICS_COUNT_CHECKIN_STATUS;

        if ( ( $museum = get_post( $place_id ) ) && ( get_museum_type() == $museum->post_type ) ) {

            $stat_record[ 'place_id' ] = $place_id; 

        } else {

            return new WP_Error(
                self::STATISTICS_ERROR_CODE_WRONG_PARAMETER_ASSINGED, 
                self::STATISTICS_ERROR_STRING_WRONG_PARAMETER_ASSINGED,
                array( 
                    self::STATISTICS_ERROR_LOCALIZED_KEY            => __( 'Place ID does not exists in the system', 'museums_network' ),
                    self::STATISTICS_ERROR_USER_SEND_INFORMATION    => $place_id
                )
            );

        }

        $old_user = $user;
        $user = is_string( $user ) ? get_user_by( 'login', $user ) : get_user_by( 'id', (int) $user ) ;

        if ( is_wp_error( $user ) ) {

            return new WP_Error( 
                self::STATISTICS_ERROR_CODE_WORNG_PARAMETER_ASSINGED, 
                self::STATISTICS_ERROR_STRING_WORNG_PARAMETER_ASSINGED, 
                array( 
                    self::STATISTICS_ERROR_LOCALIZED_KEY            => __( 'User is not exists, please check again', 'museums_network' ),
                    self::STATISTICS_ERROR_USER_SEND_INFORMATION    => $old_user
                )
            ); 

        } else {

            $stat_record[ 'user_id' ] = $user->ID;

        }

        if ( self::STATISTICS_NONE_AVAILABLE_VALUE == $checkin_timestamp ) {
            $stat_record[ 'checked_in_timestamp_gmt' ] = date('Y-m-d H:i:s', strtotime( '-7 hours' ) );
        }

        if ( null != $item_info && is_array( $item_info ) ) {

            $stat_record[ 'item_id' ] = $item_info[ self::STATISTICS_PARAM_ATTRIBUTE_ITEM_ID ] ; 

            if ( isset( $item_info[ self::STATISTICS_PARAM_ATTRIBUTE_ITEM_NAME ] ) ) {
                $stat_record[ 'item_name' ] = $item_info[ self::STATISTICS_PARAM_ATTRIBUTE_ITEM_NAME ] ; 
            }

            $stat_record[ 'statistics_type' ]   = self::STATISTICS_COUNT_FETCH_ITEM_INFORMATION_STATUS;
        }

        $return_result = array();
        if ( $wpdb->insert( $wpdb->statistics, $stat_record ) ) {
            
            $return_result = array(
                self::STATISTICS_UPDATE_MESSAGE_RESULT_KEY      => __( 'Complete update without error', 'museums_network' ),
                self::STATUSTICS_UPDATE_MESSAGE_INFORMATION_KEY => $wpdb->insert_id
            );

        } else {
            $return_result = array(
                self::STATISTICS_UPDATE_MESSAGE_RESULT_KEY      => $wpdb->print_error(),
                self::STATUSTICS_UPDATE_MESSAGE_INFORMATION_KEY => self::STATISTICS_NONE_AVAILABLE_VALUE 
            );
        }

        return $return_result ;
    }

} 

$statistics = SLStatistics::getInstance();
