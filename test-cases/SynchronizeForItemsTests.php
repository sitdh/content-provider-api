<?php

require_once '../../../../wp-load.php';
require_once ABSPATH . '/wp-includes/class-IXR.php';
require_once 'museums-network/Admin/Synchronization/FeaturedItemSynchronize.php';

class SynchronizeForItems extends PHPUnit_Framework_TestCase {

    /**
     * @group item-sync
     **/
    public function testCanGetNumberOfUnsyncItem() {
        $featured = new FeaturedItemSynchronize( 3 );

        $number = $feauted->numberOfItemForStatus( FeaturedItemSynchronize::FEATURED_ITEM_NOT_SYNC_INFORMATION_YET );
        $this->assertEquals( 6, $number );

        $client = $this->getMock( 'IXR_Client', array( 'getResponse' ), array( 'http://www.we.in.th/xmlrpc.php' ) );
        $client->expects( $this->once() )
            ->method( 'getResonse' )
            ->will( $this->returnValue( array(
                    'status'    => 'success',
                    'message'   => array(
                            'code'      => 201,
                            'message'   => 'Accepted'
                            'item_id'   => 123
                        )
                ) ) );

        $unsyncInformation = $featured->getUnsynchronizeItems();

        $this->assertEquals( 6, count( $unsyncInformation ) );

        $firstItem = $unsyncInformation[0];

        $this->assertEquals( 'FeaturedItemInformation', get_class( $firstItem ) );

        $client->query(
            'mp.sendFeaturedItem',
            $firstItem->formatted()
        );

        $resposne = $client->getResponse();

        $featured->updateItemInformation( $firstItem, $resposne );

        $savedInformation = $featured->getUnsynchronizeItems( $firstItem->itemId );

        $this->assertEquals( 123, $savedItemInformation->itemId );
    }
}
