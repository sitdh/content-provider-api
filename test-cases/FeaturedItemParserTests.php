<?php

require_once '../../../../wp-load.php';
require_once ABSPATH . '/wp-includes/class-IXR.php';
require_once 'museums-network/Admin/Informations/FeaturedItemInformationParser.php';

class FeaturedItemParserTests extends PHPUnit_Framework_TestCase
{

    /**
     * @group item-parser
     **/
    public function testManagementContextCanGetFeatureCategory() {
        $featuredContext = new FeaturedItemInformationParser;

        $item = $featuredContext->parseInformation(
            get_post( 34 ),
            array()
        );

        $itemArray = $item->formatted();

        $this->assertEquals( $item->itemTitle, $itemArray[0] );

    }
}
