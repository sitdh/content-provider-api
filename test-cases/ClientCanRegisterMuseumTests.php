<?php

require_once '../../../../wp-load.php';
require_once ABSPATH . '/wp-includes/class-IXR.php';
require_once 'museums-network/Admin/Informations/ClientMuseumDataManager.php';

class ClientCanRegisterMuseumTests extends PHPUnit_Framework_TestCase
{
    // protected $client;
    protected static $clientXMLRPC  = 'http://arokaya.local/wp/ds/xmlrpc.php';

    protected $dataManager;

    public function setUp() {
        $this->dataManager = ClientMuseumDataManager::getInstance();
    }

    public function tearDown() {
        remove_all_filters( 'option_ds_local_museum_id' );
        remove_all_filters( 'option_ds_account' );

        $this->dataManager->hasMuseumInformation = false;
        $this->dataManager->museumId = -1;
        $this->dataManager = null;
    }

    /**
     * @group initialize
     **/
    public function testDataManagerObjectsShouldEquals() {
        $anotherDataManager = new ClientMuseumDataManager;

        $this->assertEquals( $this->dataManager, $anotherDataManager, 'Data manager should singleton' );
    }

    /**
     * @group initialize
     **/
    public function testDataManagerCanGetLocalMuseumIdentity() {
        add_filter( 'option_ds_local_museum_id', function() { return -1; } );

        $this->assertEquals(
            -1,
            $this->dataManager->localMuseumId
        );

        remove_all_filters( 'option_ds_local_museum_id'  );

        add_filter( 'option_ds_local_museum_id', function() { return 179; } );

        $this->assertEquals( 179, $this->dataManager->localMuseumId );

        remove_all_filters( 'option_ds_local_museum_id'  );
    }

    /**
     * @group initialize
     **/
    public function testDataManagerCanGetMuseumIdentity() {

        $this->dataManager->defaultMuseumType = 'museum-info';

        $museumInfo = $this->dataManager->museumInformation;
        $this->assertTrue( ! empty( $museumInfo ) );

        $this->assertEquals( 79, $museumInfo->ID, "Museum's id is differrence: {$museumInfo->ID} " . gettype( $museumInfo->ID ) );

        $this->dataManager->defaultMuseumType = 'museum';
        $this->assertEquals( 'museum', $this->dataManager->defaultMuseumType, "but we've got {$this->datManager->defaultMuseumType}" );
        $museumInfo = $this->dataManager->museumInformation;

        $this->assertTrue( empty( $museumInfo ) );

        $this->dataManager->defaultMuseumType = 'museum-info';
    }

    /**
     * @group data-formatted
     *
     * @expectedException           InvalidMuseumDataManagerException
     * @expectedExceptionMessage    Museums information does not exists
     **/
    public function testDataManagerThrowExceptionWhenMuseumIdDoesNotExists() {
        // Add filter for change value if value exists
        add_filter( 'option_ds_local_museum_id', '__return_false' );

        $this->dataManager->hasMuseumInformation = false;

        $this->assertFalse( $this->dataManager->hasMuseumInformation );
        $this->assertEquals( -1, $this->dataManager->localMuseumId );

        $this->dataManager->getMuseumInformationPacked();

        // Remove filter 
        remove_filter( 'option_ds_local_museum_id', '__return_false' );
    }

    /**
     * @group data-formatted
     *
     * @expectedException           InvalidMuseumDataManagerException
     * @expectedExceptionMessage    Museums information does not exists
     **/
    public function testExceptionShouldThrowWhenHasMuseumStatusExistsButLocalMuseumWasEmpty() {
        add_filter( 'option_ds_local_museum_id', function() { return false; } );
        $this->dataManager->hasMuseumInformation = true;

        $this->dataManager->getMuseumInformationPacked();

        remove_all_filters( 'option_ds_local_museum_id' );

    }

    /**
     * @group data-formatted
     *
     * @expectedException           InvalidAccountInformationException
     * @expectedExceptionMessage    Museum pool account is invalid 
     **/
    public function testDataManagerShouldThrowExceptionWhenConnectedAccountDoesNotExists() {
        // Add filter for change value if value exists
        add_filter( 'option_ds_account', '__return_false' );

        $this->dataManager->getMuseumInformationPacked();

        // Remove filter 
        remove_filter( 'option_ds_account', '__return_false' );
    }

    /**
     * @group data-formatted
     *
     * @expectedException           InvalidMuseumDataManagerException
     * @expectedExceptionMessage    Museums information does not exists 
     **/
    public function testDataManagerCannotPackForInvalidMuseumIdentity() {
        $this->dataManager->hasMuseumInformation = true;
        add_filter( 'option_ds_local_museum_id', function() { return 13; } );

        $this->dataManager->getMuseumInformationPacked();

        $this->dataManager->hasMuseumInformation = false;
        remove_all_filters( 'option_ds_local_museum_id' );
    }

    /**
     * @group sync
     **/
    public function testDataManagerCanFormattedMuseumInformationWhenIdExists() {
        remove_all_filters( 'option_ds_account' );

        add_filter( 'option_ds_account', function() {
            return array(
                'username'  => 'lorem',
                'password'  => 'ipsum'
            );
        } );
        add_filter( 'option_ds_local_museum_id', function() { return 77; } );
        $informationPacked = get_option( 'client_datamanager' );
        add_filter( 
            'option_client_datamanager', 
            function( $informationPaced ) { 
                $informationPacked['museumId'] = -1;
                return $informationPacked;
            } 
        );
        $this->dataManager->hasMuseumInformation = true;
        $this->assertTrue( $this->dataManager->hasMuseumInformation );

        $this->dataManager->defaultMuseumType = 'museum-info';
        $this->assertEquals( 'museum-info', $this->dataManager->defaultMuseumType );

        $this->assertEquals( 77, $this->dataManager->localMuseumId );
        $this->assertEquals( 'museum-info', $this->dataManager->defaultMuseumType );

        $user_account = get_option( 'ds_account' );
        $this->assertTrue( isset( $user_account ) );
        $this->assertEquals( $user_account, $this->dataManager->connectedAccount );

        $museumData = $this->dataManager->getMuseumInformationPacked();

        $this->assertEquals( 18, count( $museumData ) );

        $this->assertEquals( 'lorem', $museumData[0] );
        $this->assertEquals( 'ipsum', $museumData[1] );
        $this->assertEquals( -1, intval( $museumData[2] ) );

        $this->assertEquals( 13.760671239462438, $museumData[7] ) ;
        $this->assertEquals( 101.11486035937503, $museumData[8] ) ;

        $this->assertEquals( 'http://arokaya.local/wp/cps/', $museumData[13] );
        $this->assertEquals( 'http://arokaya.local/wp/cps/xmlrpc.php', $museumData[14] );

        $this->assertEquals( 'public-museum', $museumData[15] );
        $this->assertEquals( 'e-museum', $museumData[16] );

        remove_all_filters( 'option_ds_local_museum_id' );
        remove_all_filters( 'option_ds_account' );
        remove_all_filters( 'option_client_datamanager' );
    }

    /**
     * @group sync
     *
     * @expectedException           InvalidConnectionException
     * @expectedExceptionMessage    Invalid connection please check it again
     **/
    public function testDataManagerShouldThrowErrorWhenEmptyConnectionAssigned() {

        $this->dataManager->performSynchronizeMuseumInformationByConnection( null );

        $lastSyncTimestamp = $this->dataManager->lastSynchronizeTimestamp;
    }

    /**
     * @group sync
     *
     * @expectedException           InvalidConnectionException
     * @expectedExceptionMessage    Invalid connection please check it again
     **/
    public function testDataManagerShouldThrowErrorWhenMissmatchConnectionAssigned() {

        $this->dataManager->performSynchronizeMuseumInformationByConnection( (object) array( 'ID' => 2 ) );

        // $lastSyncTimestamp = $this->dataManager->lastSynchronizeTimestamp;
    }

    /**
     * @group sync
     *
     * @expectedException           InvalidAccountInformationException
     * @expectedExceptionCode       403 
     * @expectedExceptionMessage    lorem ipsum 
     **/
    public function testDataManagerCanHandleForInvalidAuthorityResponse() {
        $responseResource = array(
            'status'    => 'failure',
            'message'   => array(
                'code'      => 403,
                'message'   => 'lorem ipsum'
            )
        );

        $this->dataManager->hasMuseumInformation = true;
        $this->assertTrue( $this->dataManager->hasMuseumInformation );
        $this->assertGreaterThan( 0, $this->dataManager->localMuseumId );
        
        $clientStub = $this->getMock( 'IXR_Client', array( 'getResponse' ), array( static::$clientXMLRPC ) );

        $clientStub->expects( $this->any() )
            ->method( 'getResponse' )
            ->will( $this->returnValue( $responseResource ) );

        $this->dataManager->performSynchronizeMuseumInformationByConnection( $clientStub );

        $lastSyncTimestamp = $this->dataManager->lastSynchronizeTimestamp;
    }

    /**
     * @group sync
     *
     * @expectedException           InvalidMuseumDataManagerException
     * @expectedExceptionCode       400 
     * @expectedExceptionMessage    lorem ipsum 
     **/
    public function testDataManagerCanHandleForInvalidMuseumResponse() {
        $responseResource = array(
            'status'    => 'failure',
            'message'   => array(
                'code'      => 400,
                'message'   => 'lorem ipsum'
            )
        );

        $this->dataManager->hasMuseumInformation = true;
        
        $clientStub = $this->getMock( 'IXR_Client', array( 'getResponse' ), array( static::$clientXMLRPC ) );

        $clientStub->expects( $this->any() )
            ->method( 'getResponse' )
            ->will( $this->returnValue( $responseResource ) );

        $this->dataManager->performSynchronizeMuseumInformationByConnection( $clientStub );
    }

    /**
     * @group sync
     *
     * @expectedException           InvalidConnectionException
     * @expectedExceptionMessage    Invalid connection please check it again
     **/
    public function testClientShouldThrowExceptionWhenConnectionError() {
        $this->dataManager->setServerConnection( null );
        $this->dataManager->hasMuseumInformation = true;


        $this->dataManager->performSynchronizeMuseumInformationByConnection();
    }

    /**
     * @group sync
     **/
    public function testDataManagerCanRegisterForNewsMuseum() {

        $responseResource = array(
            'status'    => 'success',
            'message'   => array(
                'code'      => 201,
                'message'   => 'Success',
                'museum_id' => 55
            )
        );

        
        $clientStub = $this->getMock( 'IXR_Client', array( 'query', 'getResponse' ), array( static::$clientXMLRPC ) );

        $clientStub->expects( $this->any() )
            ->method( 'getResponse' )
            ->will( $this->returnValue( $responseResource ) );

        $this->dataManager->hasMuseumInformation = true;
        $this->dataManager->museumId = -1;

        $this->assertTrue( $this->dataManager->hasMuseumInformation );
        $this->assertEquals( 79, $this->dataManager->localMuseumId );
        $this->assertEquals( -1, $this->dataManager->museumId );

        remove_filter( 'option_ds_local_museum_id', '__return_false' );

        $this->dataManager->performSynchronizeMuseumInformationByConnection( $clientStub );
        $museumResponse = $this->dataManager->getResponse();

        $this->assertEquals( 79, $this->dataManager->localMuseumId );
        $this->assertEquals( 55, $this->dataManager->museumId );

        $this->assertEquals( $responseResource, $museumResponse );

    }

}
