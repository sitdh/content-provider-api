<?php

require_once '../../../../wp-load.php';
require_once ABSPATH . '/wp-includes/class-IXR.php';
require_once 'museums-network/Admin/Manager/FeaturedItemsSynchronizeManager.php';

class FeaturedItemsSynchronizationTests extends PHPUnit_Framework_TestCase
{
    
    /**
     * @group featured-items-sync
     **/
    public function testFeaturedItemCanGetItemInformation() {
        $featuredItems = FeaturedItemsSynchronizeManager;

        $this->assertEquals( 'Synchronization', get_parent_class( $featuredItems ) );

        $items = $featuredItems->getRemainingInformation( 5, SynchronizationStatus::SynchronizationPostponeStatus );
        $this->assertTrue( is_array( $items ) );
        $this->assertEquals( 7, count( $items ) );

        $item = $items[0];

        $this->assertEquals( 'FeaturedItemInformation', get_class( $item ) );
        $this->assertFalse( $item->isSynchronized );
        $this->assertLessThanOrEqual( 
            SynchronizationStatus::SynchronizationPostponeStatus, 
            $item->synchronizeStatus
        );

    }
}
