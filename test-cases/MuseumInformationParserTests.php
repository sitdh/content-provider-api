<?php

require_once '../../../../wp-load.php';
require_once ABSPATH . '/wp-includes/class-IXR.php';
require_once 'museums-network/Admin/Informations/MuseumInformationParser.php';

class MuseumInformationParserTests extends PHPUnit_Framework_TestCase
{
    /**
     * @group data-formatted
     * @group museum-parser
     **/
    public function testParserCanReadMuseumInformation() {
        $parser = new MuseumInformationParser;

        add_filter(
            'option_ds_account',
            function() {
                return array(
                    'username'  => 'lorem',
                    'password'  => 'ipsum'
                );
            }
        );
        add_filter( 
            'option_client_datamanager',
            function() {
                return array(
                        'localMuseumId'         => 77,
                        'hasMuseumInformation'  => true,
                        'defaultMuseumType'     => 'museum-info'
                    );
            }
        );

        $museumInformation = $parser->parseInformation( 
            get_post( 77 ), 
            array( 'username' =>  'lorem', 'password' => 'ipsum' ),
            77 
        );

        $this->assertEquals( 'MuseumInformation', get_class( $museumInformation ) );

        $informationPacked = $museumInformation->formatted();
        $this->assertTrue( is_array( $informationPacked ) );
        
        $this->assertEquals( $informationPacked[0], $museumInformation->username );
        $this->assertEquals( $informationPacked[1], $museumInformation->password );
        $this->assertEquals( $informationPacked[2], $museumInformation->museumId );
        $this->assertEquals( $informationPacked[3], $museumInformation->museumTitle );
        $this->assertEquals( $informationPacked[4], $museumInformation->museumTitleEnglish );
        $this->assertEquals( $informationPacked[5], $museumInformation->museumContent );
        $this->assertEquals( $informationPacked[6], $museumInformation->locationDescription );
        $this->assertEquals( $informationPacked[7], $museumInformation->latitude );
        $this->assertEquals( $informationPacked[8], $museumInformation->longitude );
        $this->assertEquals( $informationPacked[9], $museumInformation->email );
        $this->assertEquals( $informationPacked[10], $museumInformation->museumStatus );
        $this->assertEquals( $informationPacked[11], $museumInformation->contactInformation );
        $this->assertEquals( $informationPacked[12], $museumInformation->groupNumber );
        $this->assertEquals( $informationPacked[13], $museumInformation->officialMuseumURL );
        $this->assertEquals( $informationPacked[14], $museumInformation->contentEndpointURL );
        $this->assertEquals( $informationPacked[15], $museumInformation->museumType );
        $this->assertEquals( $informationPacked[16], $museumInformation->exhibitionType );
        $this->assertEquals( $informationPacked[17], $museumInformation->serverThumbnailImageId );

        remove_all_filters( 'option_client_datamanager' );
        remove_all_filters( 'option_ds_account' );

    }

    /**
     * @group data-formatted
     * @group museum-parser
     **/
    public function testParserProperitesValidation() {
        $parser = new MuseumInformationParser;

        $museumInformation = $parser->parseInformation( 
            get_post( 77 ), 
            array( 'username' =>  'lorem', 'password' => 'ipsum' ),
            77 
        );

        $this->assertEquals( 'MuseumInformation', get_class( $museumInformation ) );

        $informationPacked = $museumInformation->formatted();
        $this->assertTrue( is_array( $informationPacked ) );

        $temp = $museumInformation->username;
        $this->assertFalse( empty( $temp ) );

        $temp = $museumInformation->password;
        $this->assertFalse( empty( $temp ) );

        $temp = $museumInformation->museumId;
        $this->assertTrue( is_numeric( $temp ) );

        $temp = $museumInformation->museumTitle;
        $this->assertFalse( empty( $temp ) );

        $temp = $museumInformation->museumTitleEnglish;
        $this->assertFalse( empty( $temp ) );

        $temp = $museumInformation->museumContent;
        $this->assertFalse( empty( $temp) );

        // $this->assertEquals( ! empty( $museumInformation->locationDescription ) );
        $temp = $museumInformation->latitude;
        $this->assertTrue( is_numeric( $temp ) );

        $this->assertTrue( is_numeric( $museumInformation->longitude ) );
        $this->assertEquals( 
            $museumInformation->email, 
            filter_var( $museumInformation->email, FILTER_VALIDATE_EMAIL ) 
        );

        $temp = $museumInformation->museumStatus;
        $this->assertEquals( $informationPacked[10], $temp );
        $this->assertContains( 
            $temp,
            array( 'open', 'closed', 'close-for-maintanance' )
        );

        $temp = $museumInformation->contactInformation;
        $this->assertFalse( empty( $temp ) );

        $temp = $museumInformation->groupNumber;
        $this->assertTrue( is_numeric( $temp ) );

        $temp = $museumInformation->officialMuseumURL;
        $this->assertEquals( $informationPacked[13], $temp );
        $this->assertEquals( 
            $temp,
            filter_var(
                $temp,
                FILTER_VALIDATE_URL
            )
        );

        $this->assertEquals( $inforamtionPacked[14], $museumInformation->connectionEndpointURL );
        $this->assertEquals( 
            $museumInformation->connectionEndpointURL,
            filter_var(
                $museumInformation->connectionEndpointURL,
                FILTER_VALIDATE_URL
            )
        );

        $this->assertEquals( $informationPacked[15], $museumInformation->museumType );
        $this->assertContains( 
            $museumInformation->museumType,
            array( 'public-museum', 'private-museum' )
        );

        $this->assertEquals( $informationPacked[16], $museumInformation->exhibitionType );
        $this->assertContains(
            $museumInformation->exhibitionType,
            array( 'e-museum', 'walk-in' )
        );

        $temp = $museumInformation->serverThumbnailImageId;
        $this->assertEquals( $informationPacked[17], $temp );
        $this->assertTrue( is_numeric( $temp ) );
    }

    /**
     * @group data-formatted
     * @group museum-parser
     **/
    public function testInvalidInformationHandle() {
        $parser = new MuseumInformationParser;

        $museumInformation = $parser->parseInformation( 
            get_post( 3 ),
            array( 
                'username' => 'lorem',
                'password' => 'ipsum'
            ),
            77
        );

        $emptyMuseumInfo = new MuseumInformation;
        $this->assertEquals( $emptyMuseumInfo, $museumInformation );

        remove_all_filters( 'option_client_datamanager' );
        remove_all_filters( 'option_ds_account' );
    }

}
