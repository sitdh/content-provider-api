<?php

require_once '../../../../wp-load.php';
require_once ABSPATH . '/wp-includes/class-IXR.php';
require_once 'museums-network/Admin/Informations/FeaturedItemInformation.php';

class FeaturedCommonsInformationTests extends PHPUnit_Framework_TestCase {
    
    /**
     * @group featured-item
     **/
    public function testCommonFeatureItemInformation() {
        $information = array(
            'itemTitle'         => 'Lorem ipsump',
            'itemDescription'   => 'Lorem ipsum dolor sit amet',
            'belongToMuseum'    => 5,
            'thumbnailImageId'  => 17,
            'synchronizeStatus' => 0,
            'syncTimestamp'     => null,
            'serverItemId'      => null
        );
            

        $featureInformation = new FeaturedItemInformation( $information );

        $temp = array();
        $temp[] = $featureInformation->itemTitle;
        $temp[] = $featureInformation->itemDescription;
        $temp[] = $featureInformation->belongToMuseum;
        $temp[] = $featureInformation->thumbnailImageId;
        $temp[] = $featureInformation->synchronizeStatus;
        $temp[] = $featureInformation->syncTimestamp;
        $temp[] = $featureInformation->serverItemId;

        $this->assertTrue( isset( $temp[0] ) );
        $this->assertTrue( isset( $temp[1] ) );
        $this->assertTrue( is_numeric( $temp[2] ) );
        $this->assertTrue( is_numeric( $temp[3] ) );
        $this->assertFalse( $temp[4] );
        $this->assertEmpty( $temp[5] );
        $this->assertEmpty( $temp[6] );

        $formattedInformation = $featureInformation->formatted();

        $this->assertEquals( $temp, $formattedInformation );

    }

}
