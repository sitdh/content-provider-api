<?php
/**
 * Plugin Name: Content Provider Server
 * Plugin URI: http://museums.in.th/plugins/content-provider-server
 * Description: Content Provider Server Console
 * Author: Museums Network Team
 * Version: 0.7.1b9
 * Author URI: http://museums.in.th/plugins/content-provider-server
 **/

require_once 'content-provider-config.php';
require_once 'services/content-provider-server-services.php';
require_once 'services/ds-services.php';
require_once 'admin/admin-menus.php';
require_once 'statistics/statistics-process.php';
require_once 'content-provider-db-init.php';
require_once 'content-provider-tracking.php';
require_once 'admin/lib/Admin/Statistics/Counting.php';
require_once 'admin/lib/Admin/Synchronize/ItemQueue.php';

add_action( 'cps_increase_checkin_number', array( 'Admin_Statistic_Counting', 'increaseCheckInNumberForMuseum' ), 10, 1 );
add_action( 'cps_increase_fetching_item_info_number', array( 'Admin_Statistic_Counting', 'increaseViewNumberForItem' ), 10, 2 );

function cps_setup_theme() {
}
add_action( 'after_setup_theme', 'cps_setup_theme' );

function cps_init_plugin() {
	global $CPS_CONFIG_VERSION;

	if ( version_compare( $CPS_CONFIG_VERSION, get_option( 'cps_config_version', '0.0' ), '>' ) ) 
		update_option( 'cps_config_version', $CPS_CONFIG_VERSION );

	if ( ! get_option( 'timezone_string' ) ) update_option( 'timezone_string', 'Asia/Bangkok' );

	date_default_timezone_set( get_option( 'timezone_string' ) );
    
    set_include_path( 
        get_include_path()
        . PATH_SEPARATOR
        . dirname( __FILE__ ) . '/lib/'
        . PATH_SEPARATOR
        . dirname( __FILE__ ) . '/admin/lib/'
    );

    if ( ! has_action( 'cps_consume_queue_up_feature_items', array( $this, 'consume_queue_up' ) ) ) {
        add_action( 'cps_consume_queue_up_feature_items', array( 'Admin_Synchronize_ItemQueue', 'cleanUp' ) );
        // add_action( 'cps_consume_queue_up_feature_items', function() { update_option( 'aqx', 'first-page' ); } );
    }

}
add_action( 'init', 'cps_init_plugin' );

function get_server_type() {
    $server_type = defined( MUSEUMS_CONFIG_PREFIX ) ? MUSEUMS_CONFIG_PREFIX : 'UNKNOW' ;

    return strtolower( $server_type ) ;
}

function get_museum_type() {
    return ( 'ds' == get_server_type() ) ? 'museums' : 'museum-info' ;
}

register_activation_hook( 
    __FILE__, 
    array( 
        'ContentProviderInitializer', 
        'content_provider_initialized' 
    ) 
);

register_deactivation_hook( 
    __FILE__, 
    array( 
        'ContentProviderInitializer', 
        'content_provider_cleanup' 
    ) 
);
