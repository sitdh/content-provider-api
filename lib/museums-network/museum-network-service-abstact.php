<?php
abstract class Museums_Network_XMLRPC_Abstract {
  const HTTP_STATUS_CODE_OK                     			= 200;
  const HTTP_STATUS_CODE_BAD_REQUEST            			= 400;
  const HTTP_STATUS_CODE_FORBIDDEN              			= 403;
  const HTTP_STATUS_CODE_NOT_FOUND              			= 404;
  const HTTP_STATUS_CODE_METHOD_NOT_IMPLEMENTED 			= 501;

  const MN_XMLRPC_EXCERPT_STRING_LENGHT         			= 200;
  
  const MN_XMLRPC_FAULT_CODE_BAD_REQUEST					= 4000;
  const MN_XMLRPC_FAULT_CODE_NOT_AUTHORIZED					= 4040;
  
  const MN_XMLRPC_FAULT_MESSAGE_BAD_REQUEST  				= "Bad Request";
  const MN_XMLRPC_FAULT_MESSAGE_NOT_AUTHORIZED  			= "Not Authorized";
  const MN_XMLRPC_FAULT_MESSAGE_NOT_FOUND  					= "Not found";
  const MN_XMLRPC_FAULT_MESSAGE_PLACE_ID_INVALID  			= "Place ID is invalid";
  const MN_XMLRPC_FAULT_MESSAGE_ITEM_ID_INVALID  			= "Item ID is invalid";
  const MN_XMLRPC_FAULT_MESSAGE_METHOD_NOT_IMPLEMENTED_YET	= "Method not implement yet";

  protected $_calls               = array();
  protected $_namespace           = 'mp';
  protected $_status_code         = self::HTTP_STATUS_CODE_METHOD_NOT_IMPLEMENTED ;
  protected $_need_authentication = false ;

  function __construct( $namespace = 'mp' ) {
    $this->_namespace = $namespace ;

    $reflector = new ReflectionClass( $this ) ;
    foreach( $reflector->getMethods( ReflectionMethod::IS_PUBLIC ) as $method ) {
      if ( $method->isUserDefined() && $method->getDeclaringClass()->name != get_class() ) {
        $this->_calls[] = preg_replace( '/_/', '.', $method->name );
      }
    }
    add_filter( 'xmlrpc_methods', array( $this, 'xmlrpc_register_methods' ) );
  }

  public function xmlrpc_register_methods( $methods ) {
    foreach( $this->_calls as $call ) {
      $methods[ $this->_namespace . '.' . $call ] = array( $this, 'dispatch' );
    }

    return $methods;
  }

  public function dispatch( $args ) {
    global $wp_xmlrpc_server;

    $user = '';
    $data = array();

    if ( $this->_need_authentication ) {
      if ( ! ( $user = $wp_xmlrpc_server->login( $args[0], $args[1] ) ) ) {
        return $wp_xmlrpc_server->error;
      } else {
        unset( $args[0], $args[1] );
        foreach( $args as $arg ) {
          $data[] = $arg;
        }

        $data = $user;
      }
    } else {
      $data = $args;
    }

    $call = $this->get_called_method();

    if ( method_exists( $this, $call ) ) {
      $status = call_user_func_array( array( $this, $call ), array( $data ) ) ;
    } else {
      // $status = new IXR_Error( self::HTTP_STATUS_CODE_METHOD_NOT_IMPLEMENTED, 'Method not implement yet' );
    }

    return $status; // new IXR_Error( self::HTTP_STATUS_CODE_METHOD_NOT_IMPLEMENTED, 'Method not implement yet' );
  }

  private function create_fault_message( $status ) {
    $message = '';
    switch( $status ) {

    }

    return $message;
  }

  private function get_called_method() {
    global $wp_xmlrpc_server;
    $call = $wp_xmlrpc_server->message->methodName;
    $pieces = explode( '.', $call );
    unset( $pieces[0] );
    return implode( '_', $pieces );
  }

  /**
   * Remove html tags  
   **/
  public function exclude_html_and_excerpt_chars( $some_content ) {
    $some_content = htmlspecialchars_decode( $some_content );
    $some_content = strip_tags( $some_content );
    return mb_substr( $some_content, 0, self::MN_XMLRPC_EXCERPT_STRING_LENGHT, 'UTF-8' );
  }

}
