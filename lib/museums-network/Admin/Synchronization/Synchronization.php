<?php

require_once '../Informations/Informations.php';
require_once 'SynchronizationStatus.php';

abstract class Synchronization {

    abstract public function setConnection( $connection );

    abstract public function setCredentials( $credential );

    abstract public function performSynchronize(Informations $informationPackage, array $credentials = null);

    abstract public function uploadImageWithImageId( $imageId );

}
