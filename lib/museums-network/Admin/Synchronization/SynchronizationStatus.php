<?php

class SynchronizationStatus {
    const SynchronizationUnknowStatus   = 0;
    const SynchronizationPendingStatus  = 1;
    const SynchronizationPostponeStatus = 2;
    const SynchronizationSuccessStatus  = 3;
}
