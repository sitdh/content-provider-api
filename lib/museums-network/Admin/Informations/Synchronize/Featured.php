<?php

require_once ABSPATH . WPINC . '/class-IXR.php';
require_once 'Admin/Synchronize/ItemQueue.php';

class Admin_Informations_Synchronize_Featured {

	public static function getInstance() {
        static $syncFeatured = null;

        if ( null == $syncFeatured ) {
            $syncFeatured = new Admin_Informations_Synchronize_Featured();
        }

		return $syncFeatured; 
	}

	public function __construct() {

        add_action(
            'cleanup_featured_items',
            array( $this, 'cleanup_featured_items_in_the_queue' )
        );

        if ( ! wp_next_scheduled( 'cleanup_featured_items' ) ) {
            wp_schedule_event(
                time(),
                'hourly',
                'cleanup_featured_items'
            );
        }

	}

    public function markItemsAsUpdateRequired( array $items = null ) {
        if ( null == $items ) return;

        foreach( $items as $item_id ) {
            update_post_meta( $item_id, '_cps_item_update_status', CPS_Sync_Status::CPS_ITEM_STATUS_QUEUE_UP );
            update_post_meta( $item_id, '_cps_item_action', 'update' );
            
            if ( get_post_meta( $item_id, '_cps_sync_timestamp', TRUE ) )
                delete_post_meta( $item_id, '_cps_sync_timestamp' );
        }

    }

    public function rejectThisItemSet( array $items = null ) {
        if ( null == $items ) return;

        foreach( $items as $item_id ) {
            if ( ! wp_is_post_revision( $item_id ) ) {
                wp_update_post( array(
                    'ID'            => $item_id,
                    'post_status'   => 'draft'
                ) );

                $authro_id = get_post_field( 'post_author', $item_id );

                do_action( 'notification_for_author', $author_id, $item_id );
                do_action( 'check_for_item_synchronize', $item_id );
            }

        }
    }

    public function withdrawThisItemSet( array $items = null ) {
        if ( null == $items ) return;

        foreach( $items as $item_id ) {
            update_post_meta( $item_id, '_cps_item_update_status', CPS_Sync_Status::CPS_ITEM_STATUS_QUEUE_UP );
            update_post_meta( $item_id, '_cps_item_action', 'widthdraw' );
        }
    }

    public function __call( $func_name, $args ) {

        $items = is_array( $args[0] ) ? $args[0] : explode( '|', $args[0] ) ;

        $action = '';

        if ( ! is_admin() ) wp_die( __( 'You have no authority to do this action', 'museums_network' ) );

        switch( $func_name ) {
            case 'updateFeaturedItems':
                $this->markItemsAsUpdateRequired( $items );
                break;

            case 'rejectFeaturedItems':
                $this->rejectThisItemSet( $items );
                break;

            case 'withdrawFeaturedItems':
                $action = 'withdraw';
                $this->withdrawThisItemSet( $items );
                break;

            default: 
                $this->{$func_name};
                return;
        }
    }

    public function cleanup_featured_items_in_the_queue() {
        update_option( 'performed', date('c') );
    }

	// public function updateFeaturedItems( array $featured_list = null ) {

	// 	// do_action( 'update_featured_items', implode( '|', $featured_list ) );

    //     if ( empty( $featured_list ) ) return;

    //     $value_send = implode( '|', $featured_list );

    //     if ( ! wp_next_scheduled( 'update_featured_items', $value_send ) ) {
    //         update_option( 'a_test', 'new ' . date('c') );

    //         if ( isset( $featured_list ) && null != $featured_list ) {
    //             $b = wp_schedule_single_event( 
    //                 time() + 10 * 60, 
    //                 'update_featured_items', 
    //                 array(
    //                     implode( '|', $featured_list ) 
    //                 )
    //             );

    //             echo var_dump( $b );
    //         }
    //     }
	// }

    // public function rejectFeaturedItems( $featured_list = null ) {

    // }

    // public function withdrawFeaturedItems( $featured_list = null ) {

    // }

	// public function send_information_for_update_featured_item( $featured_list = null ) {
    //     update_option( 'a_test_x', $featured_list );
    //     
	// 	// if ( null == $featured_list || ! is_array( $featured_list ) )
	// 	// 	return;

	// 	// foreach( $featured_list as $featured_item_id ) {
	// 	// 	$this->synchronized_information( $featured_item_id );
	// 	// }
	// 	
	// }

	public function synchronized_information( $featured_item_id ) {
		global $post;

		$post = get_post( $featured_item_id );

		if ( $post ) {

			update_post_meta( 
				$featured_item_id, 
				'_cps_item_update_status', 
				CPS_Sync_Status::CPS_ITEM_STATUS_SYNC_IN_PROGRESS 
			);

			$client = new IXR_Client( DISTRIBUTOR_SERVER );
			$client->debug = false;

			setup_postdata( $post );

			if ( has_post_thumbnail() ) {
				$image_id = $this->update_featured_image( get_post_thumbnail_id( get_the_ID() ), $client );
				add_post_meta( get_the_ID(), 'ds_server_image_id', $image_id );
			}

			$credentials = get_option( 'ds_account' );

			$featuredOptions = array();

			// 0: Username
			$featuredOptions[] = $credentials['username'];

			// 1: Password
			$featuredOptions[] = $credentials['password'];

			// 2: Title
			$featuredOptions[] = get_the_title();
			
			// 3: Content
			$content = get_the_content();
			$content = strip_tags( $content );
			$featuredOptions[] = mb_substr( $content, 0, 500 );

			// 4: Belong to museum id
			$featuredOptions[] = get_option( 'ds_museum_id' );

			// 5: Thumbnail Id
			$featuredOptions[] = $image_id;

			// 6: Exists id
			$featuredOptions[] = get_post_meta( get_the_ID(), 'ds_server_item_id', true );

			$client->query(
				'mp.updateFeaturedItem',
				$featuredOptions
			);

			$updateResult = $client->getResponse();

			$updateStatus = CPS_Sync_Status::CPS_ITEM_STATUS_SYNC_IN_PROGRESS;
			if ( 'success' == $updateResult['status'] ) {

				$serverItemId = $updateResult['message']['item_id'];
				update_post_meta( get_the_ID(), 'ds_server_item_id', $serverItemId );

				$updateStatus = CPS_Sync_Status::CPS_ITEM_STATUS_ALREADY_SYNC; // Success

				$sync_date = date( sprintf( 
					'%s %s',
					get_option( 'date_format' ),
					get_option( 'time_format' )
				) );
				update_post_meta( get_the_ID(), '_cps_sync_timestamp', $sync_date );

			} else {
				$updateStatus = CPS_Sync_Status::CPS_ITEM_STATUS_SYNC_ERROR; // Postpone
			}

			update_post_meta( get_the_ID(), '_cps_item_update_status', $updateStatus );

		} else {
			update_post_meta( 
				$featured_item_id, 
				'_cps_item_update_status', 
				CPS_Sync_Status::CPS_ITEM_STATUS_SYNC_ERROR 
			);
		}

		wp_reset_query();
		wp_reset_postdata();
	}



	public function update_featured_image( $post_id, $connection = null ) {

		$image_file = wp_get_attachment_image_src( $post_id, 'large' );
		$image_url = $image_file[0];

		$upload_path = wp_upload_dir();
		$image_path = str_replace( $upload_path['baseurl'], $upload_path['basedir'], $image_url );

		$image_type = wp_check_filetype( $image_path );

		$image = fopen( $image_path, 'r' );
		$image_content = fread( $image, filesize( $image_path ) );
		fclose( $image );

		$image_name = explode( DIRECTORY_SEPARATOR, $image_path );
		$image_name = array_pop( $image_name );

		$featured_image_params = array(
			'name'          => $image_name
			,'type'         => $image_type['type']
			, 'bits'        => new IXR_Base64( $image_content )
			, 'overwrite'   => false
		);

		$ds_account = get_option( 'ds_account' );

		$connection->query(
			'wp.uploadFile', 
			1, 
			$ds_account['username'], 
			$ds_account['password'], 
			$featured_image_params
		);

		$image_id = -1;

		if ( ! $connection->isError() ) {

			$uploadImageResponse = $connection->getResponse();
			$content_payload[] = $uploadImageResponse['id'];

			update_option( 'museum_image_need_update', false );
			update_option( 'museum_thumbnail_image_id', $uploadImageResponse['id'] );

			$image_id = $uploadImageResponse['id'];
		}


		return $image_id;
	}
}
