<?php

require_once 'museums-network/Admin/Informations/Informations.php';

class MuseumInformation extends Informations {

    private $valueInformation;

    private static $messageKeys = array(
            'username',
            'password',
            'museumId',
            'museumTitle',
            'museumTitleEnglish',
            'museumContent',
            'locationDescription',
            'latitude',
            'longitude',
            'email',
            'museumStatus',
            'contactInformation',
            'groupNumber',
            'officialMuseumURL',
            'contentEndpointURL',
            'museumType',
            'exhibitionType',
            'serverThumbnailImageId',
        );

    public function __construct( $init = null ) {
        $this->valueInformation = array();

        if ( $init ) {

            foreach( static::$messageKeys as $key ) {
                $this->valueInformation[$key] = @$init[$key];
            }
        }
    }

    public function formatted() {
        $msg = array(
            $this->username,
            $this->password,
            $this->museumId,
            $this->museumTitle,
            $this->museumTitleEnglish,
            $this->museumContent,
            $this->locationDescription,
            $this->latitude,
            $this->longitude,
            $this->email,
            $this->museumStatus,
            $this->contactInformation,
            $this->groupNumber,
            $this->officialMuseumURL,
            $this->contentEndpointURL,
            $this->museumType,
            $this->exhibitionType,
            $this->serverThumbnailImageId
        );
        return $msg;
    }

    public function __get( $name ) {

        if ( isset( $this->valueInformation[$name] ) ) {
            $value = $this->valueInformation[$name];
        } else {
            $value = null;
        }

        return $value;
    }

    public function __set( $name, $value ) {
        if ( array_key_exists( $name, $this->valueInformation ) ) {
            $this->valueInformation[$name] = $value;
        }
    }

}
