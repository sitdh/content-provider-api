<?php
require_once ABSPATH . '/wp-admin/includes/class-wp-list-table.php'; 
require_once 'museums-network/Admin/Informations/Synchronize/Featured.php'; 
require_once 'Admin/Synchronize/ItemQueue.php'; 

class Admin_Informations_FeaturedItemList extends WP_List_Table {

	public function __construct() {
		parent::__construct(
			array(
				'singular'	=> 'featured_item',
				'plural'	=> 'featured_items',
				'ajax'		=> false
			)
		);
	}

    public function get_featured_items() {
		$featured_item_category_number = get_option( 'cps_featured_category' );

        $paged = $this->get_pagenum();
        $per_page = 10;

		$featured_items = new WP_Query(array(
			'cat'	            => $featured_item_category_number,
			'paged'	            => $paged,
            'posts_per_page'    => 10
		));

        $featured_items_packed = array();

        if ( $featured_items->have_posts() ) {

            while( $featured_items->have_posts() ) {
                $featured_items->the_post();

                $items = array();
                $items['col_featured_id'] = get_the_ID();
                $items['col_featured_title'] = get_the_title();
                $items['col_featured_description'] = get_the_excerpt();
                $items['col_featured_author'] = get_the_author();

                $sync_status = get_post_meta( get_the_ID(), '_cps_item_update_status', true ) ;
                $sync_status = isset( $sync_status ) ? $sync_status : CPS_Sync_Status::CPS_ITEM_STATUS_NOT_SYNC_YET ;

                $items['col_featured_sync_status'] = CPS_Sync_Status::cps_status_message( $sync_status );

                $items['col_featured_sync_timestamp'] = get_post_meta( get_the_ID(), '_cps_sync_timestamp', true );

                $featured_items_packed[] = $items;
            }

        }


        $this->set_pagination_args( array(
            'total_items'   => $featured_items->found_posts,
            'per_page'      => 10
        ) );

        return $featured_items_packed;
    }

	public function prepare_items() {
		global $wpdb, $_wp_column_headers;

		$coloums = $this->get_columns();
		$sortable = array(); // $this->get_sortable_columns();
		$hidden = array( 'col_featured_id' );

		$page = filter_input( INPUT_GET, 'page', FILTER_SANITIZE_STRIPPED );
		$paged = filter_input( INPUT_GET, 'paged', FILTER_SANITIZE_NUMBER_INT );

		$this->_column_headers = array( 
			$this->get_columns(), 
			$hidden,
            $sortable
		);

		$this->process_bulk_action();

		$screen = get_current_screen();


        $this->items = $this->get_featured_items();
	}

	public function get_bulk_actions() {
		return array(
				'synchronize'   => __( 'Synchronize', 'museums_network' ),
				'reject'        => __( 'Reject', 'museums_network' ),
				'withdraw'      => __( 'Withdraw', 'museums_network' ),
			);
	}

	public function process_bulk_action() {

        $nonce = '';
        $nonce_action = '';

        $action = '';

        $items_id = null;

        if ( isset( $_GET['_wpnonce'] ) ) {
            $nonce = filter_input( INPUT_GET, '_wpnonce', FILTER_SANITIZE_STRING );
            $nonce_action = $this->_args['singular'];

            if ( ! wp_verify_nonce( $nonce, $nonce_action ) ) {
                wp_die( 'Security check failed!' );
            }

            $items_id = array( $_REQUEST['item_id'] );

        } elseif ( isset( $_POST['_wpnonce'] ) ) {
            $nonce = filter_input( INPUT_POST, '_wpnonce', FILTER_SANITIZE_STRING );
			$nonce_action = "bulk-{$this->_args['plural']}";

            if ( ! wp_verify_nonce( $nonce, $nonce_action ) ) {
                wp_die( 'Security check failed!' );
            }

            $items_id = $_REQUEST['featured_item'];
        }

		switch( $this->current_action() ) {
			case 'synchronize':
                Admin_Informations_Synchronize_Featured::getInstance()->updateFeaturedItems( $items_id );
				break;

			case 'reject':
                Admin_Informations_Synchronize_Featured::getInstance()->rejectFeaturedItems( $items_id );
				break;

			case 'withdraw':
                Admin_Informations_Synchronize_Featured::getInstance()->withdrawFeaturedItems( $items_id );
				break;

			default:
				return;
				break;
		}

        if ( ! wp_next_scheduled( 'cps_consume_queue_up_feature_items' ) ) {
            wp_schedule_single_event( time() + 60, 'cps_consume_queue_up_feature_items' );
        }

		return;
	}

	/** Data Source **/
	public function column_default( $item, $column_name ) {
		$item_value = '';

		switch( $column_name ) {
			case 'col_featured_id':
				$item_value = sprintf( 
					'<input type="checkbox" name="selected_featured_id[]" value="%d" />', 
					$item['col_featured_id'] 
				);
				break;

			// case 'col_featured_title':
			// 	$item_value = sprintf(
			// 		'<a href="%1$s" title="%2$s">%2$s</a>',
			// 		get_permalink( $item['col_featured_id'] ),
			// 		$item['col_featured_title']
			// 	);
			// 	break;

			case 'col_featured_sync_status':
				$item_value = $item[$column_name];
				if ( $item['col_featured_sync_timestamp'] ) 
					$item_value .= sprintf( 
						'<br /><small>(%s)</small>', 
						$item['col_featured_sync_timestamp']
					);
				break;

			case 'col_featured_description':
			case 'col_featured_author':
				$item_value = $item[$column_name];
				break;

			default:
				$item_value = '-';
		}

		return $item_value;
	}

    public function column_cb( $item ) {
        return sprintf( 
            '<input type="checkbox" name="%s[]" value="%s" />',
            $this->_args['singular'],
            $item['col_featured_id']
        );
    }

    public function column_col_featured_title( $item ) {

        $nonce = wp_create_nonce( $this->_args['singular'] );

        $action_template = '<a href="?page=content-provider-menu&tab=cps-featured&action=%1$s&item_id=%2$s&_wpnonce=%5$s" title="%3$s">%4$s</a>';
        $actions = array(
                'sync' => sprintf( 
                        $action_template,
                        'synchronize',
                        $item['col_featured_id'],
                        __( 'Synchronize this item to server', 'museums_network' ),
                        __( 'Synchronize', 'museums_network' ),
                        $nonce
                    ),
                'reject' => sprintf( 
                        $action_template,
                        'reject',
                        $item['col_featured_id'],
                        __( 'Reject and change status to pending', 'museums_network' ),
                        __( 'Reject', 'museums_network' ),
                        $nonce
                    ),
                'withdraw' => sprintf( 
                        $action_template,
                        'pullback',
                        $item['col_featured_id'],
                        __( 'Withdraw this item back from server', 'museums_network' ),
                        __( 'Withdraw', 'museums_network' ),
                        $nonce
                    ),
            );

        $linked_item = sprintf(
            '<a href="%1$s" title="%2$s" target="_blank">%2$s</a>',
            get_permalink( $item['col_featured_id'] ),
            $item['col_featured_title']
        );

        return sprintf( 
            '%s %s',
            $linked_item,
            $this->row_actions( $actions )
        );

    }

	public function no_items() {
		_e( 'No featured items', 'museums_network' );
	}

	public function get_columns() {
		return $columns = array(
            'cb'                            => '<input type="checkbox" />',
			'col_featured_id'				=> __( 'ID', 'museums_network' ),
			'col_featured_title'			=> __( 'Title', 'museums_network' ),
			'col_featured_description'		=> __( 'Description', 'museums_network' ),
			'col_featured_author'			=> __( 'Author', 'museums_network' ),
			'col_featured_sync_status'		=> __( 'Sync. status', 'museums_network' )
		);

		return $columns;
	}

}
