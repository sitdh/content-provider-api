<?php

require_once 'Parser.php';
require_once 'Informations.php';
require_once 'MuseumInformation.php';

class MuseumInformationParser extends Parser {

    public function parseInformation( $collection_info, array $connectedAccount, $museumId = -1, $post_type = 'museum-info' ) {
        global $post;

        if ( is_numeric( $collection_info ) ) {
            $collection_info = get_post( $collection_info ) ;

            if ( ! $collection_info ) {
                return new MuseumInformation;
            }
        } elseif ( ! is_object( $collection_info ) ) {
            return new MuseumInformation;
        }

        if ( $post_type != get_post_field( 'post_type', $collection_info->ID ) ) {
            return new MuseumInformation;
        }

        $post = $collection_info;
        setup_postdata( $post );

        $museumMetadata = get_post_meta( get_the_ID() );

        $featureItemDictionary = array();

        $featureItemDictionary['username'] = @$acount['username'];
        $featureItemDictionary['password'] = @$acount['password'];

        $featureItemDictionary['museumId'] = empty( $museumInfo['museumId'] ) ? -1 : $museumInfo['museumId'] ;

        $featureItemDictionary['username'] = $connectedAccount['username'];
        $featureItemDictionary['password'] = $connectedAccount['password'];
        $featureItemDictionary['museumId'] = $museumId;
        $featureItemDictionary['museumTitle'] = get_the_title();

        // English title
        $featureItemDictionary['museumTitleEnglish'] = isset( $museumMetadata['museum_title_english'] ) ? array_shift( $museumMetadata['museum_title_english'] ) : '' ;

        $featureItemDictionary['museumContent'] = '';
        // Museum description
        // -- remove all html tags
        $featureItemDictionary['museumContent'] = strip_tags( get_the_content() );

        // Location
        // -- description
        $featureItemDictionary['locationDescription'] = isset( $museumMetadata['location_description'] ) ? array_shift( $museumMetadata['location_description'] ) : '' ;

        // -- latitude
        $featureItemDictionary['latitude'] = isset( $museumMetadata['latitude'] ) ? array_shift( $museumMetadata['latitude'] ) : 13.75 ;

        // -- longitude
        $featureItemDictionary['longitude'] = isset( $museumMetadata['longitude']) ? array_shift( $museumMetadata['longitude'] ) : 100.466667 ;

        // Email
        $featureItemDictionary['email'] = isset($museumMetadata['museum_email']) ? array_shift( $museumMetadata['museum_email'] ) : get_option( 'admin_email' ) ;

        // Museum Service status
        $featureItemDictionary['museumStatus'] = isset($museumMetadata['museum_status']) ? array_shift( $museumMetadata['museum_status'] ) : 'open' ;

        // Contact information
        $featureItemDictionary['contactInformation'] = isset($museumMetadata['museum_connect']) ? array_shift( $museumMetadata['museum_connect'] ) : get_option( 'admin_email' ) ;

        // Category selected
        $featureItemDictionary['groupNumber'] = isset($museumMetadata['cps_museum_category_selected']) ? array_shift( $museumMetadata['cps_museum_category_selected'] ) : -1 ;

        // Public website
        $featureItemDictionary['officialMuseumURL'] = isset($museumMetadata['official_website']) ? array_shift( $museumMetadata['official_website'] ) : get_option( 'url' ) ;

        // Content provider
        $featureItemDictionary['contentEndpointURL'] = isset($museumMetadata['museum_content_provider']) ? array_shift( $museumMetadata['museum_content_provider'] ) : get_option( 'url' ) . '/xmlrpc.php' ;

        // Museum type
        $featureItemDictionary['museumType'] = isset($museumMetadata['cps_exhibition_status']) ? array_shift( $museumMetadata['cps_exhibition_status'] ) : 'public-museum' ;

        // Exhibition style
        $featureItemDictionary['exhibitionType'] = isset($museumMetadata['cps_exhibition_type']) ? array_shift( $museumMetadata['cps_exhibition_type'] ) : 'walk-in' ;

        // Image id on server
        $featureItemDictionary['serverThumbnailImageId'] = isset($museumMetadata['museum_thumbnail_image_id']) ? array_shift( $museumMetadata['museum_thumbnail_image_id'] ) : -1 ;

        wp_reset_postdata();

        return new MuseumInformation( $featureItemDictionary, 'y' );
    }

    public function save() {

    }

}
