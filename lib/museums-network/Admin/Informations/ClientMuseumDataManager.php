<?php
require_once 'museums-network/Admin/Exceptions/ClientExceptions.php';
require_once 'museums-network/Admin/Informations/MuseumInformationParser.php';
require_once 'museums-network/Admin/Informations/MuseumInformation.php';

class ClientMuseumDataManager {

    private $museumOptions;

    private $previousResponse;

    private $serverConnection;

    private static $defaultOptionDataSourceKey = 'client_datamanager';

    public function __construct(){

        $this->museumOptions = get_option( static::$defaultOptionDataSourceKey );

        if ( empty( $this->museumOptions ) ) {
            $this->museumOptions['defaultMuseumType'] = 'museum-info';
            
            $posts = get_posts(array(
                'post_type'         => $this->museumOptions['defaultMuseumType'],
                'posts_per_page'    => 1,
                'post_status'       => 'any'
            ));

            $this->hasMuseumInformation = ( 1 == $posts->found_posts ) ? true : false ;

        }

        $this->serverConnection = null;

    }

    public static function getInstance() {
        static $staticDataManager = null;
        if ( null === $staticDataManager ) {
            $staticDataManager = new ClientMuseumDataManager;
        }

        return $staticDataManager;
    }

    public function __get( $key ) {
        $value = false;

        switch( $key ) {

            case 'localMuseumId':
                $value = get_option( 'ds_local_museum_id' ) ? get_option( 'ds_local_museum_id' ) : -1 ;
                break;

            case 'museumInformation':
                $value = null;
                if ( $this->defaultMuseumType == get_post_field( 'post_type', $this->localMuseumId ) ) {
                    $value = get_post( $this->localMuseumId ) ;
                }
                break;

            case 'connectedAccount':
                $value = get_option( 'ds_account' );
                break;

            case 'defaultMuseumType':
                if ( ! array_key_exists( $key, $this->museumOptions ) ) {
                    $this->defaultMuseumType = 'museum-info';
                    $this->museumOptions[$key] = 'museum-info';
                } 

                $value = $this->museumOptions[$key];
                break;

            case 'lastSynchronizedTime':
                if ( ! array_key_exists( $key, $this->museumOptions ) ) {
                    $this->lastSynchronizedTime = false;
                    $this->museumOptions[$key] = false;
                }

                $value = $this->museumOptions[$key];

                break;

            default:
                $value = isset( $this->museumOptions[$key] ) ? $this->museumOptions[$key] : -1 ;
        }

        return $value;

    }

    public function __set( $key, $value ) {
        switch( $key ) {
            case 'localMuseumId':
            case 'connectedAccount':
                update_option( $key, $value );
                break;

            default:
                $this->museumOptions[$key] = $value;
        }

        $this->refreshConfigurationOptions();
    }

    public function setServerConnection( $client ) {
        $this->serverConnection = $client ;
    }

    public function getMuseumInformationPacked() {
        global $post; 

        if ( ! get_option( 'ds_account' ) ) {
            throw new InvalidAccountInformationException( 'Museum pool account is invalid' );
        }

        if ( $this->hasMuseumInformation ) {
            if ( -1 == $this->localMuseumId ) {
                throw new InvalidMuseumDataManagerException( 'Museums information does not exists' );
            }

            if ( $this->defaultMuseumType != get_post_field( 'post_type', $this->localMuseumId ) && -1 != $this->localMuseumId ) {
                throw new InvalidMuseumDataManagerException( 'Museums information does not exists' );
            }
        } else {
            throw new InvalidMuseumDataManagerException( 'Museums information does not exists' );
        }

        $resposneInformation = MuseumInformationParser::getInstance()
                ->parseInformation( 
                    $museumInformation, 
                    $this->connectedAccount, 
                    $this->museumId 
                )
                ->formatted();

        return $responseInformation;
    }

    public function performSynchronizeMuseumInformationByConnection( $client = null ) {

        $this->serverConnection = empty( $client ) ? $this->serverConnection : $client ;

        if ( empty( $this->serverConnection )
            || ! ( method_exists( $this->serverConnection, 'query' ) 
                || method_exists( $this->serverConnection, 'getResponse') ) )  
        {
            throw new InvalidConnectionException( 'Invalid connection please check it again' );
        }

        $this->serverConnection->query(
            'mp.updateMuseumsInformation',
            $this->getMuseumInformationPacked()
        );

        $response = $this->serverConnection->getResponse();
        if ( 403 == $response['message']['code'] ) {
            throw new InvalidAccountInformationException( 
                $response['message']['message'], 
                $response['message']['code'] 
            ); 
        } elseif ( 400 == $response['message']['code'] ) {
            throw new InvalidMuseumDataManagerException( 
                $response['message']['message'], 
                $response['message']['code'] . ' ddadfas;dfljas;dfl' 
            ); 
        }

        $this->museumId             = $response['message']['museum_id'];
        $this->hasMuseumInformation = true;

        $this->previousResponse     = $response;

    }

    public function getResponse() {
        return $this->previousResponse;
    }

    /**
     * Private method
     **/
    private function refreshConfigurationOptions() {
        update_option( static::$defaultOptionDataSourceKey, $this->museumOptions );
    }

}
