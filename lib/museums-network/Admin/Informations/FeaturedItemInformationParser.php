<?php

require_once '../Synchronizztion/SynchronizationStatus.php';

class FeaturedItemInformationParser extends Parser {

    public function parseInformation($collection, array $connectedAccount, $featuredGroup = -1, $museumType = 'featured-item' ) {
        global $post;

        if ( is_numeric( $collection ) ) {
            $collection = get_post( $collection );

            if ( ! $collection ) return new FeaturedItemInformation;

        } elseif ( ! is_object( $collection ) ) {
            return new FeaturedItemInformation;
        }

        $post = $collection;
        setup_postdata( $post );

        $featuredInformationPack = array();

        $featuredInformationPack['itemTitle']           = get_the_title();
        $featuredInformationPack['itemDescription']     = strip_tags( get_the_content() );
        $featuredInformationPack['belongToMuseum']      = get_option( 'ds_museum_id', -1 );
        $featuredInformationPack['thumbnailImageId']    = get_post_thumbnail_id();

        $temp = get_post_meta( get_the_ID(), 'ds_is_synchronized', true ); 
        $temp = ! empty( $temp ) ;
        $featuredInformationPack['isSynchronized']      = $temp;

        $featuredInformationPack['syncTimestamp']       = get_post_meta( get_the_ID(), 'ds_synchronized_timstamp', true );
        $featuredInformationPack['serverItemId']        = get_post_meta( get_the_ID(), 'ds_server_item_id', true );

        $featuredInformationPack['synchronizeStatus']   = get_post_meta( get_the_ID(), 'ds_synchronize_status', SynchronizationStatus::SynchronizationPostponseStatus );
        
        wp_reset_postdata();
        
        return new FeaturedItemInformation( $featuredInforamtionPack );
    }

    public function save() {

    }
}
