<?php

abstract class Parser {
    protected static $instance;

    final public static function getInstance() {
        $class = get_called_class();

        if ( ! static::$instance[$class] ) {
            static::$instance[$class] = new $class();
        }

        return static::$instance[$class];
    }

    abstract public function parseInformation($objectInfo, array $conectionAccount, $museumId = -1, $post_type = 'museum-info' );

    abstract public function save();
}
