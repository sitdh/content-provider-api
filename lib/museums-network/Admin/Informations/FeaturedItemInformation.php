<?php


class FeaturedItemInformation extends Informations
{
    private static $featuredKeys = array(
            'itemTitle',
            'itemDescription',
            'belongToMuseum',
            'thumbnailImageId',
            'synchronizeStatus',
            'syncTimestamp',
            'serverItemId'
        );

    private $featuredElements;

    public function __construct( $init = null ) {
        $this->featuredElements = array();

        if ( $init ) {

            foreach( static::$featuredKeys as $key ) {
                $this->featuredElements[$key] = @$init[$key];
            }
        }
    }

    public function __get( $name ) {
        $value = null;

        if ( isset( $this->featuredElements[$name] ) ) {
            $value = $this->featuredElements[$name];
        }

        return $value;
    }

    public function __set( $name, $value ) {
        if ( array_key_exists( $name, $this->featuredElements ) ) {
            $this->featuredElements[$name] = $value;
        }
    }

    public function formatted() {
        $items = array(
                    $this->itemTitle,
                    $this->itemDescription,
                    $this->belongToMuseum,
                    $this->thumbnailImageId,
                    $this->synchronizeStatus,
                    $this->syncTimestamp,
                    $this->serverItemId
                );
        return $items;
    }

}
