<?php

class MediaManagement {
    
    public static function uploadImageWithImageId( $imageId, $connection, $credentials ) {

        if ( ! class_exists( 'IXR_Base64' ) ) require_once ABSPATH . '/wp-includes/class-IXR.php';

        $image_file = wp_get_attachment_image_src( $imageId, 'large' );
        $image_file_path = $image_file[0];

        $upload_path = wp_upload_dir();
        $image_file_path = str_replace( $upload_path['baseurl'], $upload_path['basedir'], $image_file_path );

        $image_type = wp_check_filetype( $image_file_path );

        $image_handle = fopen( $image_path, 'r' );
        $image_content = fread( $image_handle, filesize( $image_file_path ) );
        fclose( $image_handle );

        $image_name = explode( DIRECTORY_SEPARATOR, $image_file_path );
        $image_name = array_pop( $image_name );

        $image_upload_options = array(
            'name'      => $image_name,
            'type'      => $image_type,
            'bits'      => new IXR_Base64( $image_content ),
            'overwrite' => false
        );

        $connection->query(
            'wp.uploadFile',
            1,
            @$credentials['username'],
            @$credentials['password'],
            $image_upload_options
        );

        $uploadResponse = array(
            'status'    => 'error',
            'image_id'  => -1
        );
        if ( ! $connection->isError() ) {
            $response = $connection->getResponse();

            $uploadResponse['status'] = 'success';
            $uploadResponse['image_id'] = $response['id'];
        }

        return (object) $uploadResponse;
    }
}
