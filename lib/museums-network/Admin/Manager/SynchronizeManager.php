<?php

require_once '../Synchronization/SynchronizationStatus.php';

abstract class SynchronizeManager {

    const SynchronizeService = "mp.updateFeaturedInformation";

    abstract public function getRemainingInformation( $inGroupId, $forStatus = SynchronizationStatus::SynchronizationPostponseStatus );
}

