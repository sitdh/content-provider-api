<?php

// Prepare for WordPress's environment
$root_dir_path = dirname( __FILE__ );
require_once $root_dir_path . '/../../../../wp-load.php' ;

// Prepare for Old version 

if ( file_exists( '../content-provider-config.php' ) ) {

    require_once '../content-provider-config.php' ;
    require_once 'content-provider-server-services.php' ;

    define( 'DEFAULT_ACTION_NAMESPACE', CPS_SERVICE_NAMESPACE );

} elseif ( file_exists( '../distributor-config.php' ) ) {

    require_once '../distributor-server-config.php' ;
    require_once 'distributor-server-services.php' ;

    define( 'DEFAULT_ACTION_NAMESPACE', DS_SERVICE_NAMESPACE );

}

if ( ! defined( 'DEFAULT_ACTION_NAMESPACE' ) ) 
    define( 'DEFAULT_ACTION_NAMESPACE', 'mp' );

if ( class_exists( 'IXR_Error' ) ) 
    require_once ABSPATH . '/wp-includes/class-IXR.php' ;

class CPS_JSON_Converter {
    var $core_information;

    private static $message_provider = null;

    private static $json_provider = null ;

    public function __construct() {

    }

    public static function getInstance() {
        if ( ! self::$json_provider ) {
            if ( class_exists( 'Content_Provider_XMLRPC_Services' ) ) {
                self::$message_provider = new Content_Provider_XMLRPC_Services( DEFAULT_ACTION_NAMESPACE ) ;
            } elseif ( class_exists( 'Distributor_Server_API' ) ) {
                self::$message_provider = new Distributor_Server_API( DEFAULT_ACTION_NAMESPACE ) ;
            }

            self::$json_provider = new CPS_JSON_Converter() ;
        }

        return self::$json_provider ;
    }

    public function handleMessageRequest( $request_params ) {

        if ( empty( $request_params[ 'action' ] ) ) {
            return new IXR_Error(
                Museums_Network_XMLRPC_Abstract::HTTP_STATUS_CODE_BAD_REQUEST,
                "Please specific action for this request"
            );
        }

        $action = $request_params[ 'action' ] ;

        $action_to_perform = $this->parse_action_to_method( $action ) ;

        if ( is_object( $action_to_perform ) && 'IXR_Error' == get_class( $action_to_perform ) )
            return $action_to_perform ;

        $params = array() ;

        switch ( $action ) {
            case DEFAULT_ACTION_NAMESPACE . '.user.profile':
            case DEFAULT_ACTION_NAMESPACE . '.user.visit':
            case DEFAULT_ACTION_NAMESPACE . '.user.watched':
            case DEFAULT_ACTION_NAMESPACE . '.user.bookshelf':
                $params = array( 
                    @$request_params[ 'username' ],
                    @$request_params[ 'password' ],
                );

                break;

            case DEFAULT_ACTION_NAMESPACE . '.user.register':
                $params = array(
                    @$request_params[ 'username' ],
                    @$request_params[ 'password' ],
                    @$request_params[ 'email' ],
                    @$request_params[ 'name' ],
                    @$request_params[ 'surname' ],
                    @$request_params[ 'age_range' ],
                );

                break;

            case DEFAULT_ACTION_NAMESPACE . '.places.museum':
                $params = array(
                    @$request_params[ 'place_id' ],
                    @$request_params[ 'username' ],
                );

                break;

            case DEFAULT_ACTION_NAMESPACE . '.place.museum.items':
            case DEFAULT_ACTION_NAMESPACE . '.place.museum.items.featured':
            case DEFAULT_ACTION_NAMESPACE . '.place.museum.exhibition.zones.items':
                $params = array(
                    @$request_params[ 'place_id' ],
                    @$request_params[ 'zone_id' ],
                    @$request_params[ 'item_id' ],
                    @$request_params[ 'pace_number' ],
                );

                break;

            case DEFAULT_ACTION_NAMESPACE . '.places.news':
                $params = array(
                    @$request_params[ 'place_id' ],
                    @$request_params[ 'paged' ],
                );

                break;

            case DEFAULT_ACTION_NAMESPACE . '.place.museum.exhibition.zones':
                $params = array(
                    @$request_params[ 'place_id' ],
                );

                break;

                $params = array(
                    @$request_params[ 'place_id' ],
                    @$request_params[ 'zone_id' ],
                );

                break;

            case DEFAULT_ACTION_NAMESPACE . '.version':
            case DEFAULT_ACTION_NAMESPACE . '.place.profile':
            default:
                break;
        }

        if ( is_callable( array( self::$message_provider, $action_to_perform ) ) ) {

            $result = call_user_func_array( 
                            array(
                                self::$message_provider,
                                $action_to_perform
                            ),
                            array( $params )
                        );

            if ( empty( $result ) ) {
                $result = new IXR_Error(
                    200,
                    "Empty information"
                );
            }

        } else {

            $result = new IXR_Error( 
                Museums_Network_XMLRPC_Abstract::HTTP_STATUS_CODE_BAD_REQUEST,
                "Invalid method name"
            );

        }

        return $result ;
    }

    public static function parse_action_to_method( $action ) {
        if ( false === strpos( $action, DEFAULT_ACTION_NAMESPACE ) ) {
            return new IXR_Error(
                Museums_Network_XMLRPC_Abstract::HTTP_STATUS_CODE_BAD_REQUEST,
                "Invalid method name"
            ) ;
        }

        $function_name = str_replace( DEFAULT_ACTION_NAMESPACE . '.', '', $action ) ;
        $function_name = str_replace( '.', '_', $function_name ) ;

        return $function_name ;
    }
}

// if request empty, return some error message
if ( empty( $_POST ) ) 
    die( 'POST method is required' );

$json_object = CPS_JSON_Converter::getInstance()->handleMessageRequest( $_POST );

header( 'Content-Type: application/json; charset=utf-8' ) ;
echo json_encode( $json_object );
