<?php
if ( ! class_exists( 'IXR_Client' ) ) require_once ABSPATH . WPINC . '/class-IXR.php';

add_action( 'wp_ajax_ds_authentication', 'ds_authentication' );
add_action( 'wp_ajax_ds_update_account', 'ds_authentication' );
function ds_authentication( $args ) {
	$username = @$_POST['data']['username'];
	$password = @$_POST['data']['password'];

	$client = new IXR_Client( DISTRIBUTOR_SERVER );

	$return_msg = array(
		'status' 		=> 'failed'
		,'message' 	=> ''
	);

	if ( ! $client->query( 'wp.getProfile', 2, $username, $password ) ) {
		$return_msg['message'] = 'Username or password invalid';
		$return_msg['x'] = $_POST;
	} else {
		$return_msg['status'] = 'success';
		$return_msg['message'] = $client->getResponse();
	}

	if ( 'ds_update_account' == @$_POST['action'] ) {
		update_option( 'ds_account', array( 'username' => $username, 'password' => $password ) );
	}

	header( 'Content-Type: application/json;' );
	echo json_encode( $return_msg );
	die();
		
}
