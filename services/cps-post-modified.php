<?php
add_filter( 'the_posts', 'modified_post_for_sticky_first' );
function modified_post_for_sticky_first( $posts ) {
	$stickies = array();
	foreach( $posts as $i => $post ) {
		if ( is_sticky( $post->ID ) ) {
			$stickies[] = $post;
			unset($posts[$i]);
		}
	}

	return array_merge( $stickies, $posts );
}

?>
