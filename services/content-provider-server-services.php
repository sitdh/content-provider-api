<?php 

if ( ! defined( 'CPS_CONFIG_PREFIX' ) )
    require_once dirname( __FILE__ ) . '/../content-provider-config.php' ;

if ( ! class_exists( 'Museums_Network_XMLRPC_Abstract' ) )
	require_once 'museums-network/museum-network-service-abstact.php';

if ( ! class_exists( 'IXR_Error' ) ) 
    require_once ABSPATH . '/wp-includes/class-IXR.php' ;

class Content_Provider_XMLRPC_Services extends Museums_Network_XMLRPC_Abstract {
	protected $user;

	/**
	 * Method name: ${prefix}.version
	 *
	 * Return: Version's information
	 */
	public function version() {
		global $CPS_CONFIG_VERSION;

		return array(
			'version' => sprintf(
				'DEMO-%s-%s'
				, CPS_CONFIG_PREFIX
				, $CPS_CONFIG_VERSION	
			)
			, get_option( 'cps_config_version' )	
			, 'type' => CPS_CONFIG_SERVER_TYPE
		);
	}

	/**
	 * Method name: ${prefix}.user.profile
	 *
	 * Return: User's profile
	 */
	public function user_profile( $args ) {
		return $this->_auth( $args ) ;
	}

	/**
	 * Method name: ${prefix}.user.register
	 *
	 * Return: User's profile
	 */
	public function user_register( $args ) {

		$client = new IXR_Client( DISTRIBUTOR_SERVER );

		if ( ! $client->query( 'mp.user.register', $args ) ) {
			return new IXR_Error(
				$client->getErrorCode()
				, $client->getErrorMessage()
			);
		}

		return $client->getResponse();
	}

	/**
	 * Method name: ${prefix}.user.visit
	 *
	 * Return: Visited place status
	 */
	public function user_visit( $args ) {
		
		if ( is_array( $args ) || ( 2 != count( $args ) ) ) {
			$message = new IXR_Error(
				self::HTTP_STATUS_CODE_BAD_REQUEST
				, self::MN_XMLRPC_FAULT_MESSAGE_BAD_REQUEST
			);
		}

	}
	
	/**
	 * Method name: ${prefix}.user.watched
	 *
	 * Return: User's watched list
	 **/
	public function user_watched( $args ) {
		$user = $this->_auth( $args );
		if ( is_a( $user, 'IXR_Error' ) ) 
			return $user;
			
		$watched = array();
		if ( $watched_list = get_user_meta( $user->ID, 'watched', true ) ) { 
			foreach( $watched_list as $place_id ) {
				$watched[] = $this->_get_place_information( $place_id );
			}
		}
		
		return $watched;
	}
	
	/**
	 * Method name: ${prefix}.user.bookshelf
	 *
	 * @return User's bookshelf
	 **/
	public function user_bookshelf( $args ) {
		$user = self::_auth( $args );
		
		$bookshelf = array();
		if ( $bookshelf_list = get_user_meta( $user->ID, 'bookshelf', true ) ) {
			foreach( $bookshelf_list as $place_id => $items ) {
				foreach( $items as $item ) {
					// get item info
				}
			}
		}
		
		return $bookshelf;
	}
	
	/**
	 * ======================= Museum =======================
	 **/

	/**
	 * ======================= Place Profile =======================
	 **/
	public function place_profile( $args ) {

		global $post;

		$museum_info = get_posts(array(
			'post_type' => 'museum-info'
			,'numberposts' => 1
			,'post_status' => 'any'
		));

		if ( $museum_info ) {
			$post = array_pop( $museum_info );
			unset( $museum_info );

			setup_postdata( $post );
			
			$museum_info = array();

			$museum_info['place_id'] 			= get_option( 'ds_museum_id' );
			$museum_info['name'] 				= get_the_title();
			$museum_info['description'] 		= get_the_content();
			$museum_info['create_timestamp'] 	= get_post_time('c', true, get_the_ID());
			$museum_info['add_timestamp'] 		= get_post_time('c', true, get_the_ID());

			$museum_info['location']			= array(
													'description' => get_post_meta( get_the_ID(), 'location_description', true )
													,'latitude'   => get_post_meta( get_the_ID(), 'latitude', true )
													,'longitude'  => get_post_meta( get_the_ID(), 'longitude', true )
												);

			if ( has_post_thumbnail() ) {
				$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ) ) ;
				$museum_info['thumbnail']['location'] 	= $thumbnail[0];
				$museum_info['thumbnail']['width'] 		= $thumbnail[1];
				$museum_info['thumbnail']['height'] 	= $thumbnail[2];
			}

			$museum_info['website'] 			= get_post_meta( get_the_ID(), 'official_website', true );
			$museum_info['local_connection'] 	= get_post_meta( get_the_ID(), 'cps_website', true );
			$museum_info['email'] 				= get_post_meta( get_the_ID(), 'email', true );
			$museum_info['open_date'] 			= get_post_meta( get_the_ID(), 'cps_servicetime', true );
			$museum_info['status'] 				= get_post_meta( get_the_ID(), 'cps_museum_status', true );
			$museum_info['exhibition_status'] 	= get_post_meta( get_the_ID(), 'cps_exhibition_status', true );
		}

		wp_reset_postdata();

		return $museum_info;
	}
	 
	/**
	 * Method name: ${prefix}.places.museum
	 *
	 * @return Museum Information
	 **/
	public function places_museum( $args ) {

        if ( is_array( $args ) ) {
            $place_id   = ( $tmp = array_shift( $args ) ) ? intval( $tmp ) : -1 ; 
            $user_name  = ( $tmp = array_shift( $args ) ) ? $tmp : -1 ;
        } else {
            $place_id   = $args ;
            $user_name  = -1 ;
        }

        if ( $user_name ) {
            do_action( 'cps_increase_checkin_number', 1 );
        }

		if ( 0 >=  $place_id ) 
			return new IXR_Error(
				Museums_Network_XMLRPC_Abstract::HTTP_STATUS_CODE_FORBIDDEN
				, 'Please see information from Distributor Server'
			);

		if ( 0 <= $user_name ) {
			$this->_update_user_visit( $place_id, $username, $password );
		}

		return array( 'places' => $this->_get_place_information( $place_id ) ) ;
	}
	
	/**
	 * Method name: ${prefix}.places.news
	 *
	 * @return Exhibition news 
	 */
	public function places_news( $args ) {

		$place_id = empty( $args ) ? 1 : $args ;

		$paged = 0;
		
		query_posts(array(
			'category_name' => 'news',
			'posts_per_page' => 10,
			'paged' => $paged
		));
		
		$events_news = array();
		if ( have_posts() ) {
			while( have_posts() ) { the_post();
				$event = array();
				$event['event_id'] = get_the_ID();
				$event['display_name'] = strip_tags( htmlspecialchars_decode( get_the_title() ) );
				$event['description'] = strip_tags( htmlspecialchars_decode( get_the_content() ) );
				
				$event['place_info'] = $this->_get_place_information( $place_id ) ;
				
				if ( $start_date = get_post_meta( get_the_ID(), 'start_date', true ) ) {
					$event['start_date'] = $start_date;
				}
				
				if ( $start_date = get_post_meta( get_the_ID(), 'end_date', true ) ) {
					$event['end_date'] = $start_date;
				}
				
				if ( $start_date = get_post_meta( get_the_ID(), 'event_type', true ) ) {
					$event['event_type'] = $start_date;
				}
				
				if ( $start_date = get_post_meta( get_the_ID(), 'flee', true ) ) {
					$event['flee'] = $start_date;
				}
				
				$event['web_site'] = get_post_meta( get_the_ID(), 'web_site', true ) ? get_post_meta( get_the_ID(), 'web_site', true ) : get_bloginfo( 'url' ) ;
				
				$event['email'] = get_post_meta( get_the_ID(), 'email', true ) ? get_post_meta( get_the_ID(), 'email', true ) : get_bloginfo( 'admin_email' ) ;
				
				if ( $start_date = get_post_meta( get_the_ID(), 'phone_number', true ) ) {
					$event['phone_number'] = $start_date;
				}
				
				$events_news[] = $event;
			}
		}
		
		return $events_news;
	}

	/**
	 * Method name: ${prefix}.place.museum.items
	 *
	 * @return Items information
	 */
	public function place_museum_items( $args, $featured = false, $backward_compatible = false ) {
        global $statistics;

        $item_name = -1;

        if ( is_array( $args ) ) {
            $place_id 		= ( $tmp = @array_shift( $args ) ) ? $tmp : -1 ;
            $zone_id 		= ( $tmp = @array_shift( $args ) ) ? $tmp : -1 ;
            $item_id 		= ( $tmp = @array_shift( $args ) ) ? $tmp : -1 ;
            $page_number    = ( $tmp = @array_shift( $args ) ) ? $tmp :  1 ;

            if ( $item_id ) {
                $user_id = ( $tmp = @array_shift( $args ) ) ? $tmp : -1 ;
            }

        } else {
            $place_id       = $args ;
            $zone_id        = -1 ;
            $item_id        = -1 ;
            $page_number    = -1 ;
            $user_id        = -1 ;
        }

		$message = array();

        // remove fixed place-info out
		if ( $place_id == get_option( 'ds_museum_id' ) ) {

			if ( 0 < $item_id ) {
				// specific item id
				$item = $this->_get_item_information( $item_id, $place_id, $featured, $backward_compatible, true, $zone_id ); 

                if ( $backward_compatible ) {

                    $message[] = $item ;

                } else {

                    if ( is_array( $item ) ) {

                        $message[ 'number_of_page'] = 1;
                        $message[ 'number_of_item'] = 1;

                        $message[ 'item_info'][] = $item;

                        $item_name = $item['display_name'];

                    } else {
                        $message = $item;
                    }

                }

                do_action( 'cps_increase_fetching_item_info_number', $item_id, 1 );

			} else {

				$cat = ( $featured ) ? get_option( 'cps_featured_category' ) : get_option( 'cps_items_category' ) ;
                
				$items = new WP_Query(array(
                    'cat' => $cat, 
                    'paged' => $page_number
				));

				if ( $items->have_posts() ) {

                    if ( ! $backward_compatible ) {
                        $message['number_of_page'] = intval( $items->max_num_pages ) ;
                        $message['number_of_item'] = intval( $items->found_posts );
                    } 

					while( $items->have_posts() ) {
						$items->the_post();

                        if ( $backward_compatible ) {
                            $message[] = $this->_get_item_information( get_the_ID(), $place_id, $featured, $backward_compatible, $zone_id );
                        } else {
                            $message['item_info'][] = $this->_get_item_information( get_the_ID(), $place_id, $featured, $backward_compatible, $zone_id );
                        }
					}

				} else {
					$message = new IXR_Error(
						self::HTTP_STATUS_CODE_NOT_FOUND
						, self::MN_XMLRPC_FAULT_MESSAGE_NOT_FOUND
					);
				}
			}

		} else {
			$message = new IXR_Error(
				self::HTTP_STATUS_CODE_BAD_REQUEST
				, self::MN_XMLRPC_FAULT_MESSAGE_BAD_REQUEST
			);
		}

        if ( $item_id && $user_id && ( 1 == count( $message['item_info'] ) ) ) {
            do_action( 'cps_increase_fetching_item_info_number', $item_id, 1 );
        }

		return $message;
	}

	public function place_museum_items_featured( $args, $is_feature = true, $backward_compatible = false ) {
		return $this->place_museum_items( $args, $is_feature, $backward_compatible );
	}
	
	public function place_museum_exhibition_zones( $args ) {
		if ( empty( $args ) ) {
			return new IXR_Error(
				self::MN_XMLRPC_FAULT_CODE_BAD_REQUEST,
				self::MN_XMLRPC_FAULT_MESSAGE_BAD_REQUEST
			);
		}
		
		$place_id = is_array( $args ) ? $args[0] : $args ;

		
		$exhibition_zones = array();

		if ( get_option( 'ds_museum_id' ) == $place_id ) {
			
			$cat = get_term_by( 'id', get_option( 'cps_items_category', 1 ), 'category' );
			if ( $cat ) {
				$zone_id = $cat->term_id;
				$zones_list = get_categories(array(
					'child_of' => $zone_id,
					'hide_empty' => 0
				));
				
				foreach( $zones_list as $zone ) {
					$exhibition_zones[] = array(
						'id' => $zone->cat_ID,
						'url' => get_category_link( $zone->cat_ID ),
						'name' => $zone->cat_name,
						'description' => strip_tags( htmlspecialchars_decode( $zone->category_description ) )
					);
				}
			}

			$exhibition_zones['exhibition_zone'] = $exhibition_zones;

		} else {
			$exhibition_zones = new IXR_Error(
				self::HTTP_STATUS_CODE_METHOD_NOT_IMPLEMENTED, 
                self::MN_XMLRPC_FAULT_MESSAGE_METHOD_NOT_IMPLEMENTED_YET
			);
			
		}
		
		return $exhibition_zones;
	}
	
	public function place_museum_exhibition_zones_items( $args ) {
		return $this->place_museum_items_featured( $args, false, true ) ;
	}
	
	/**
	 * ========================== Private method ========================== 
	 **/
	
	private function _auth( $args ) {

		$client = new IXR_Client( DISTRIBUTOR_SERVER );

		$username = array_shift( $args );
		$password = array_shift( $args );

		if ( ! $client->query( 'mp.user.profile', array( $username, $password ) ) ) {
			return new IXR_Error(
				$client->getErrorCode()
				, $client->getErrorMessage()
			);
		}

		return $client->getResponse();
	}
	
	private function _update_user( $action, $user_id, $content_type, $content_id ) {
		if ( $action == 'visit_info' ) {
		
			$visited_place_list = get_user_meta( $user_id, 'visited_list', true );
			/**
			 * visited_place_list => (
			 *   place_id => visit_count
			 * )
			 **/
			 
			$new_visited_place_list = $visited_place_list ;
			
			if ( array_key_exists( $content_id, $new_visited_place_list ) ) {
				$new_visited_place_list[ $content_id ]++;
			} else {
				$new_visited_place_list[ $content_id ] = 1;
			}
			
			update_user_meta(
				$user_id,
				'visited_list',
				$new_visited_place_list,
				$visited_place_list
			);
			
		}
		
	}
	
	private function _get_place_information( $place_id ) {
	
		if ( $place_id != get_option( 'ds_museum_id' ) ) {
			return new IXR_Error(
				self::HTTP_STATUS_CODE_METHOD_NOT_IMPLEMENTED
				, self::MN_XMLRPC_FAULT_MESSAGE_METHOD_NOT_IMPLEMENTED_YET
			);
		}

		$query = get_posts(array( 
            'post_type' 		=> 'museum-info',
            'post_status' 	    => 'any',
		));

		if ( $p = @$query[0] ) { 

			$description 									= strip_tags( htmlspecialchars_decode( $p->post_content ) );
			$short_description 								= mb_substr( $description, 0, 140, 'UTF-8' );

			$place_information['place_id'] 					= $place_id ;
			$place_information['name'] 						= $p->post_title ;
			$place_information['description'] 				= $description ; // strip_tags( htmlspecialchars_decode( $p->post_content ) ) ;
			$place_information['short_description'] 		= $short_description ; // mb_substr( strip_tags( htmlspecialchars_decode( $p->post_conten ) ), 0, 140, 'UTF-8' ) ;
			$place_information['create_timestamp'] 			= $p->post_date_gmt ;
			$place_information['add_timestamp'] 			= $p->post_date_gmt ;
			$place_information['location']['description'] 	= get_post_meta( $p->ID, 'location_description', true ) ;
			$place_information['location']['latitude'] 		= get_post_meta( $p->ID, 'latitude', true ) ;
			$place_information['location']['longitude'] 	= get_post_meta( $p->ID, 'longitude', true ) ;
			$place_information['website'] 					= get_post_meta( $p->ID, 'official_website', true ) ;
			$place_information['local_connection'] 			= get_bloginfo( 'url' ) . '/xmlrpc.php'; 
			$place_information['email'] 					= get_post_meta( $p->ID, 'museum_email', true ) ;

			$service_time 									= get_post_meta( $p->ID, 'cps_servicetime', true );

			$days = array();
			foreach( $service_time as $dayname => $day ) {
				if ( 'open' == @$day['status'] ) {
					$dayname = date( 'l', strtotime( $dayname ) );
					$days[$dayname] = array(
						'open_time' 	=> sprintf( '%s:%s', substr( $day['open_time'], 0, 2 ), substr( $day['open_time'], -2 ) )
						,'close_time' => sprintf( '%s:%s', substr( $day['close_time'], 0, 2 ), substr( $day['close_time'], -2 ) )
					);
				}
			}

			$place_information['open_date'] = $days;

			$museum_type = get_post_meta( $p->ID, 'cps_museum_status', true );

			switch( $museum_type ) {
				case 'open':
				case 'closed':
					$museum_type = ucfirst( $museum_type );
					break;

				case 'close-for-maintenance':
					$museum_type = 'Close for Maintenance';
					break;
			}

			$place_information['status'] = $museum_type;

			$museum_type = get_post_meta( $p->ID, 'cps_exhibition_status', true );
			$museum_type = str_replace( '-', ' ', $museum_type );
			$place_information['exhibition_status'] = ucfirst( $museum_type ) ;		
		}
	
		return $place_information;
	}
	
	private function _get_item_information( $item_id, $place_id = 0, $is_featured = false, $backward_compatible = false, $gain_counter = false, $zone_id = -1 ) {
		$item_info = array();

		if ( $item_record = get_post( $item_id ) ) {

            // add more statistic
            if ( $gain_counter ) {
                update_post_meta(
                    $item_id,
                    'item_countable_view',
                    1 + intval( get_post_meta( $item_id, 'item_countable_view', true ) )
                );
            }

            if ( $backward_compatible ) {
                $item_info['id'] = $item_record->ID;
            } else {
                $item_info['item_id'] = $item_record->ID;
            }

			$item_info['display_name'] = strip_tags( htmlspecialchars_decode($item_record->post_title ) );
			
			$item_info['description'] = strip_tags( htmlspecialchars_decode( $item_record->post_content ) );
			
			if ( has_post_thumbnail( $item_id ) ) {
				$post_thumbnail = get_post_thumbnail_id( $item_id );

				$p_thumbnail = get_post( $post_thumbnail );
				
				$m = array();
				$m['type'] = 'image';
				list( $m['location'], $m['width'], $m['height'] ) = wp_get_attachment_image_src( $post_thumbnail, 'large' );
				$m['description'] = strip_tags( htmlspecialchars_decode( $p_thumbnail->post_content ) );
				
				$item_info['thumbnail'] = $m;

				unset( $p_thumbnail );
			}
			
			if ( ! $is_featured ) {
				$medias = get_posts(array(
                    'posts_per_page'    => -1,
					'post_type'         => 'attachment',
					'post_parent'       => $item_id,
				));

				if ( $medias ) {
					$media_list = array();
					foreach( $medias as $media ) { 
						$m = array();
						list( $m['type'], $_ ) = explode( '/', $media->post_mime_type );

						if ( $m['type'] == 'image' ) {
							list( $m['location'], $m['width'], $m['height'] ) = wp_get_attachment_image_src( $media->ID, 'large' );
						} else {
							$m['location'] = wp_get_attachment_url( $media->ID );
						}
						
						$m['description'] = strip_tags( htmlspecialchars_decode( $media->post_content ) );
						
						$media_list[] = $m;
						
						unset( $m );
					}
					
					$item_info['medias'] = $media_list ;
				}
				
				unset( $medias );
			}
			
			if ( has_tag() ) {
				$tags = wp_get_post_tags( $item_id );
				foreach( $tags as $tag ) {
					$item_info['keywords'][] = $tag->name;
				}
			}
			
			$item_info['dated_added'] = $item_record->post_date;
			
            if ( ! $backward_compatible ) {
                $item_info['exhibition_place'] = $this->_get_place_information( get_option( 'ds_museum_id' ) );
            }

		} else {
			$item_info = new IXR_Error( 
				self::HTTP_STATUS_CODE_NOT_FOUND
				, self::MN_XMLRPC_FAULT_MESSAGE_ITEM_ID_INVALID
			);
		}
		
		return $item_info;
	}
}

new Content_Provider_XMLRPC_Services( CPS_SERVICE_NAMESPACE );

if ( ! function_exists( 'switch_to_blog' ) ) :
	function switch_to_blog() {
		return false;
	}
endif;
