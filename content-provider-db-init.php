<?php

class ContentProviderInitializer {

	protected static $contentDBVersion = '1.0';

    public static function content_provider_initialized() {
        global $wpdb;

        update_option( 'cps_table_version', static::$contentDBVersion );

        $wpdb->statistics = $wpdb->prefix . 'statistics';

        $statistic_control = sprintf( 'CREATE TABLE IF NOT EXISTS `%s` (
                            `statistics_id` bigint(20) NOT NULL AUTO_INCREMENT,
                            `user_id` bigint(20) NOT NULL,
                            `place_id` bigint(20) NOT NULL,
                            `item_id` bigint(20) DEFAULT NULL,
                            `item_name` varchar(250) DEFAULT NULL,
                            `statistics_type` int(1) DEFAULT NULL,
                            `checked_in_timestamp_gmt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            PRIMARY KEY (`statistics_id`)
                        )',
                        $wpdb->statistics
                    );

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $statistic_control );
    }

    public static function content_provider_cleanup() {
        global $wpdb;

        delete_option( 'cps_table_version' );

        $wpdb->query( "DROP TABLE IF EXISTS {$wpdb->statistics}" );
    }
}
