<?php

set_include_path( get_include_path() . PATH_SEPARATOR . plugin_dir_path( __FILE__ ) . 'lib/' );

$CPS_CONFIG_VERSION = '0.7.1b9';

if ( ! defined( 'DISTRIBUTOR_SERVER' ) ) {
	define( 'DISTRIBUTOR_SERVER', 'http://www.museums.in.th/xmlrpc.php' );
	// define( 'DISTRIBUTOR_SERVER', 'http://arokaya.local/wp/ds/xmlrpc.php' );
}

if ( ! defined( 'DISTRIBUTOR_SERVER_URL' ) ) {
	define( 'DISTRIBUTOR_SERVER_URL', 'http://www.museums.in.th/' );
	// define( 'DISTRIBUTOR_SERVER_URL', 'http://arokaya.local/wp/ds' );
}

define( 'MUSEUMS_CONFIG_PREFIX', 'CPS' );
define( 'CPS_CONFIG_PREFIX', 'CPS' );
define( 'CPS_CONFIG_SERVER_TYPE', 'Content Distributor Server' );
	
define( 'CPS_SERVICE_NAMESPACE', 'mp' );

if ( ! defined( 'CPS_SYNC_METHOD_NOT_SYNC_DATA' ) )
	define( 'CPS_SYNC_METHOD_NOT_SYNC_DATA', 0 );

if ( ! defined( 'CPS_SYNC_METHOD_HOURLY_SYNC' ) )
	define( 'CPS_SYNC_METHOD_HOURLY_SYNC', 1 );

if ( ! defined( 'CPS_SYNC_METHOD_DAILY_SYNC' ) )
	define( 'CPS_SYNC_METHOD_DAILY_SYNC', 2 );

if ( ! defined( 'CPS_SYNC_METHOD_WEEKLY_SYNC' ) )
	define( 'CPS_SYNC_METHOD_WEEKLY_SYNC', 3 );

if ( ! defined( 'CPS_SYNC_METHOD_MANUALLY_SYNC' ) )
	define( 'CPS_SYNC_METHOD_MANUALLY_SYNC', 4 );

if ( ! defined( 'CPS_SYNC_METHOD_CUSTOM_TIME_SYNC' ) )
	define( 'CPS_SYNC_METHOD_CUSTOM_TIME_SYNC', 5 );

if ( ! defined( 'CPS_SYNC_METHOD_IMMEDIATELY_SYNC' ) )
	define( 'CPS_SYNC_METHOD_IMMEDIATELY_SYNC', 6 );

class CPS_Sync_Status {
	const CPS_ITEM_STATUS_NOT_SYNC_YET 			= 0;
	const CPS_ITEM_STATUS_SYNC_IN_PROGRESS 	    = 1;
	const CPS_ITEM_STATUS_ALREADY_SYNC 			= 2;
	const CPS_ITEM_STATUS_SYNC_ERROR 			= 3;
	const CPS_ITEM_STATUS_QUEUE_UP 			    = 4;

	static function cps_status_message( $status = self::CPS_ITEM_STATUS_NOT_SYNC_YET ) {
		$status_message = '';

		switch( $status ) {
			case self::CPS_ITEM_STATUS_NOT_SYNC_YET:
				$status_message = __( 'Not sync yet', 'museums_network' );
				break;

			case self::CPS_ITEM_STATUS_SYNC_IN_PROGRESS:
				$status_message = __( 'Sync in progress', 'museums_network' );
				break;

			case self::CPS_ITEM_STATUS_ALREADY_SYNC:
				$status_message = __( 'Already sync', 'museums_network' );
				break;

			case self::CPS_ITEM_STATUS_SYNC_ERROR:
				$status_message = __( 'Error while sync', 'museums_network' );
				break;

            case self::CPS_ITEM_STATUS_QUEUE_UP:
				$status_message = __( 'Queue up', 'museums_network' );
				break;
		}

		return $status_message;
	}
}

class CPS_Distributor_Server_Connection {
	const CPS_DS_CONNECTION_UNKNOW_CONNECTION					= 'unknow';
	const CPS_DS_CONNECTION_SYNC_IN_PROGRESS					= 'sync-in-progress';
	const CPS_DS_CONNECTION_CONNECTED 							= 'connected';
	const CPS_DS_CONNECTION_DISCONNECTED 						= 'disconnected';
	const CPS_DS_CONNECTION_ERROR_WHILE_CONNECTION				= 'error_while_connection';

	static function localized_server_connection_message( $server_status = self::CPS_DS_CONNECTION_UNKNOW_CONNECTION ) {
		$localized_message = '';
		switch( $server_status ) {
			case self::CPS_DS_CONNECTION_UNKNOW_CONNECTION:
				$localized_message = __( 'Unknow', 'museums_network' );
				break;

			case self::CPS_DS_CONNECTION_SYNC_IN_PROGRESS:
				$localized_message = __( 'Sync in progress', 'museums_network' );
				break;

			case self::CPS_DS_CONNECTION_CONNECTED:
				$localized_message = __( 'Connected', 'museums_network' );
				break;

			case self::CPS_DS_CONNECTION_DISCONNECTED:
				$localized_message = __( 'Disconnected', 'museums_network' );
				break;

			case self::CPS_DS_CONNECTION_ERROR_WHILE_CONNECTION:
				$localized_message = __( 'Error while connection', 'museums_network' );
				break;

		}

		return $localized_message;
	}
}

class CPS_Registration_Status {
	const CPS_REGISTRATION_NOT_REGISTER_STATUS 			= 'not-register-yet';
	const CPS_REGISTRATION_APPROVED_STATUS				= 'approved';
	const CPS_REGISTRATION_PENDING_FOR_APPROVAL_STATUS	= 'pending-for-approval';
	const CPS_REGISTRATION_DISAPPROVAL_STATUS 			= 'disapproval';
}

